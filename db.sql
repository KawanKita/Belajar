-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Okt 2016 pada 10.54
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_categories`
--

CREATE TABLE `cms_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(64) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT NULL,
  `tree_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_categories`
--

INSERT INTO `cms_categories` (`id`, `category`, `description`, `parent_id`, `tree_id`) VALUES
(1, 'Politik', NULL, NULL, '001'),
(2, 'Partai', NULL, 1, '001001'),
(3, 'Ekonomi', NULL, NULL, '002'),
(4, 'Bisnis', NULL, 3, '002001'),
(5, 'UKM', NULL, 3, '002002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_comments`
--

CREATE TABLE `cms_comments` (
  `id` int(11) NOT NULL,
  `comment_date` date DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `comment` longtext,
  `status` varchar(32) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_admin` varchar(32) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(64) CHARACTER SET big5 DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_comments`
--

INSERT INTO `cms_comments` (`id`, `comment_date`, `type`, `relation_id`, `name`, `email`, `comment`, `status`, `parent_id`, `is_admin`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, '2016-10-20', 'pengaduan', NULL, 'seftian@mail,com', 'seftian@mail,com', 'Untuk pelelangan harap di perbaharui sistimnya', 'appr', NULL, 'false', 'seftian@mail,com', '2016-10-19 17:00:00', NULL, NULL),
(2, '2016-10-20', 'pengaduan', NULL, 'avivuv@gmail.com', 'avivuv@gmail.com', 'webnya jangan dihapus lagi', 'appr', NULL, 'false', 'avivuv@gmail.com', '2016-10-19 17:00:00', NULL, NULL),
(3, '2016-10-20', 'pengaduan', NULL, 'Developer', 'uone.developer@gmail.com', 'iya mas, maaf kemarin masih ada gangguan', 'appr', 2, '1', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(4, '2016-10-20', 'pengaduan', NULL, 'Developer', 'uone.developer@gmail.com', 'Untuk pelayanan sistem baru sudah diterapkan mulai bulan ini', 'appr', 1, '1', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(5, '2016-10-20', 'pengaduan', NULL, 'seftian@mail.com', 'seftian@mail.com', 'Proses lelang awal bulan tolong ditinjau ulang', 'appr', NULL, 'false', 'seftian@mail.com', '2016-10-19 17:00:00', NULL, NULL),
(6, '2016-10-20', 'pengaduan', NULL, 'Developer', 'uone.developer@gmail.com', 'Peninjauan ulang akan kami lakukan sesegera mungkin', 'appr', 5, '1', 'developer', '2016-10-19 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_options`
--

CREATE TABLE `cms_options` (
  `id` int(11) NOT NULL,
  `group` varchar(32) DEFAULT NULL,
  `key` varchar(64) DEFAULT NULL,
  `value` text,
  `description` text,
  `relation` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_options`
--

INSERT INTO `cms_options` (`id`, `group`, `key`, `value`, `description`, `relation`) VALUES
(1, 'theme', 'theme_main_slider', '4', NULL, 'post_title'),
(2, 'theme', 'theme_main_gallery', '5', NULL, 'post_title');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_posts`
--

CREATE TABLE `cms_posts` (
  `id` int(10) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `content` longtext,
  `main_image` text,
  `format` varchar(32) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_posts`
--

INSERT INTO `cms_posts` (`id`, `title`, `post_date`, `content`, `main_image`, `format`, `type`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Pilihan Gubernur DKI', '2016-10-01', '<p>Dengan adanya 3 pasangan bakal calon, Pilgub DKI 2017 diprediksi bakal berlangsung dua putaran. Bagaimana elektabilitas masing-masing pasangan calon bila diadu head to head di putaran kedua?<br />\r\n<br />\r\nSaiful Mujani Research Center (SMRC) melakukan survei elektabilitas para calon bila Pilgub DKI 2017 dilakukan saat ini. Responden juga diminta menyuarakan pilihan apabila hanya ada dua calon di antara Anies Baswedan-Sandiaga Uno, Basuki Tjahaja Purnama (Ahok)-Djarot Saiful Hidayat, atau Agus Yudhoyono-Sylviana Murni.<br />\r\n<br />\r\n&quot;Simulasi dua pasangan: Ahok-Djarot 49,5%, Agus-Sylvi 35,1%, belum jawab 15,4%,&quot; kata Direktur SMRC, Sirojudin Abbas dalam rilis Survei SMRC di Hotel Sari Pan Pacific, Jakarta Pusat, Kamis (20/10/2016).<br />\r\n<br />\r\nBagaimana bila Ahok-Djarot dan Anies-Sandi yang melenggang ke putaran kedua? Ternyata Ahok-Djarot tetap unggul.<br />\r\n<br />\r\n&quot;Simulasi dua pasangan: Anies-Sandi 36,9%, Ahok-Djarot 47,9%, tidak jawab 15,1%,&quot; jelasnya.<br />\r\n<br />\r\nYang menjadi pertanyaan adalah bagaimana jika petahana Ahok-Djarot gagal di putaran pertama. Siapa di antara Agus-Sylvi dan Anies-Sandi yang bisa meraup suara?<br />\r\n<br />\r\n&quot;Simulasi 2 pasangan: Agus-Sylvi 37,2%, Anies-Sandi 34%, tidak jawab 28,8%,&quot; ungkap Sirajudin.<br />\r\n<br />\r\nDi survei yang dipublikasikan SMRC, elektabilitas Agus-Sylvi memang merangkak naik dan menyalip Anies-Sandi. Meski begitu, semua kemungkinan masih terbuka apalagi terkait satu atau dua putaran.<br />\r\n<br />\r\n&quot;Masih ada kemungkinan berjalan 1 putaran, 2 putaran juga masih ada. Sekarang simulasi pada keadaan terkini dahulu,&quot; ujar Sirojudin.<br />\r\n<br />\r\nPenelitian dilakukan pada 1-9 Oktober 2016. Populasi survei adalah warga DKI yang sudah berusia di atas 17 tahun. Dalam survei ini, jumlah sampel acak sebanyak 810 orang yang dipilih secara multistage random sampling.<br />\r\n<br />\r\nResponden diwawancara secara tatap muka dan margin of error sebesar 3,9% pada tingkat kepercayaan 95%. Quality control dilakukan secara random ke 20% secara random terhadap total sampel. Dalam quality control, tak ditemukan kesalahan berarti.</p>\r\n', NULL, 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(2, 'Jokowi Ajak Gubernur se-Indonesia Cari Solusi Berantas Pungli di', '2016-10-20', '<p><strong>Jakarta</strong> - Presiden Joko Widodo mengumpulkan para gubernur seluruh Indonesia di Istana Negara. Jokowi ingin membahas solusi pemberantasan pungutan liar (pungli) yang masih marak di daerah.<br />\r\n<br />\r\nJokowi mengatakan masalah pungli ini sudah lama terjadi di Indonesia. Untuk itu, memang sudah seharusnya kebiasaan ini dihapuskan.<br />\r\n<br />\r\n&quot;Pungli ini sudah bertahun-tahun. Dan kita menganggap itu sebuah hal yang normal-normal saja, yang biasa saja. Dan, kita permisif terhadap pungli itu. Oleh sebab itu, pada hari ini saya mengajak para gubernur untuk membicarakan langkah-langkah konkret di daerah dalam rangka pemberantasan pungutan liar,&quot; kata Jokowi di hadapan para gubernur di Istana Negara, Kompleks Istana Kepresidenan, Jakarta Pusat, Kamis (20/10/2016).<br />\r\n<br />\r\nJokowi mengatakan pungli ini bukan urusan seberapa besar nominalnya. Melainkan, pungli ini bisa berdampak buruk pada kepercayaan terhadap birokrasi dan perekonomian rakyat.<br />\r\n<br />\r\n&quot;Tidak hanya urusan KTP, tidak hanya urusan sertifikat, tidak hanya urusan izin-izin, tidak hanya urusan yang ada di jalan raya, tidak hanya urusan yang berkaitan dengan di pelabuhan, kantor, instansi. Bahkan di rumah sakit. Hal-hal apa pun yang berkaitan dengan pungutan tidak resmi, harus kita bersama-sama untuk mulai kurangi dan mulai hilangkan,&quot; katanya.<br />\r\n<br />\r\n&quot;Dan dengan keterpaduan itulah, kita harapkan operasi pemberantasan pungli akan semakin efektif,&quot; tambah Jokowi.</p>\r\n', 'http://localhost/p2bj/public/source/47e9da63-e4a6-4467-90de-050f2bd3c615.jpg', 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 04:59:47'),
(3, 'Bank Indonesia resmi turunkan suku bunga acuan jadi 4,75 persen', '2016-10-19', '<p><strong>Merdeka.com - </strong>Bank Indonesia memutuskan untuk menurunkan suku bunga acuan yaitu BI 7 Day Reverse Repo Rate sebesar 25 bps ke level 4,75 persen dengan Suku Bunga Deposit Facility (DF) turun 25 bps menjadi 4 persen dan Lending Facility (LF) juga diturunkan sebesar 25 bps menjadi 5,5 persen.</p>\r\n\r\n<p>&quot;Penurunan suku bunga ini efektif 20 Oktober 2016,&quot; ujar Direktur Eksekutif Departemen Komunikasi BI, Tirta Segara di kantornya, <strong><a href="http://www.merdeka.com/tag/j/jakarta/">Jakarta</a></strong>, Kamis (20/10).</p>\r\n\r\n<p>Menurutnya, penurunan ini dilakukan untuk meningkatkan efektivitas transmisi kebijakan moneter.</p>\r\n\r\n<p>Terhitung mulai Agustus 2016, BI menggunakan BI 7 Day Reverse Repo Rate sebagai suku bunga kebijakan menggantikan BI Rate.</p>\r\n', 'http://localhost/p2bj/public/source/bank-indonesia-resmi-turunkan-suku-bunga-acuan-jadi-475-persen.jpg', 'standart', 'info', 'publish', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 04:30:05'),
(4, 'Main Slider', '2016-10-20', NULL, 'http://localhost/p2bj/public/source/1.jpg', 'image', 'page', 'publish', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 04:44:17'),
(5, 'Main Gallery', '2016-10-20', '<div>\r\n<h2>&nbsp;</h2>\r\n&nbsp;\r\n\r\n<div>\r\n<div>\r\n<div>&nbsp;</div>\r\n</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>\r\n<div>\r\n<div>&nbsp;</div>\r\n</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>\r\n<div>&nbsp;</div>\r\n</div>\r\n\r\n<div>&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n', NULL, 'image', 'page', 'draft', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 05:11:54'),
(6, 'Dua tahun Jokowi-JK, DPR sebut kesenjangan sosial masih tinggi', '2016-10-18', '<p><strong>Merdeka.com - </strong>Ketua Komisi VIII DPR, Ali Taher Parasong, memberikan catatan dua tahun kepemimpinan Presiden <strong><strong><a href="http://profil.merdeka.com/indonesia/j/joko-widodo/berita/">Joko Widodo</a></strong></strong> dan Wakil Presiden <strong><strong><a href="http://profil.merdeka.com/indonesia/m/muhammad-jusuf-kalla/berita/">Jusuf Kalla</a></strong></strong>. Dia menilai tingginya kesenjangan sosial masih menjadi persoalan harus diselesaikan pemerintah.</p>\r\n\r\n<p>Menurutnya, pembangunan infrastruktur yang gencar dilakukan pemerintah seharusnya diimbangi dengan sisi ekonomi masyarakat. Ali mendapat data dari Badan Pusat Statistik (BPS). menyebutkan masih ada 27 juta orang miskin harus mendapatkan bantuan.<br />\r\n<br />\r\n&quot;Iya kemiskinan masih banyak, pembangunan infrastruktur harus dilakukan, ekonomi juga harus, tapi juga kesenjangan sosial harus dikurangin dong,&quot; kata Ali di Komplek DPR, Senayan, <strong><a href="http://www.merdeka.com/tag/j/jakarta/">Jakarta</a></strong> Pusat, Kamis (18/10).<br />\r\n<br />\r\nOleh sebab itu, politikus Partai Amanat Nasional (PAN) itu meminta pemerintah terus berupaya mengeluarkan masyarakat dari kemiskinan dan kesenjangan sosial. Dia juga menyoroti pentingnya revitalisasi penguatan UU dan kelembagaan serta pengawasan anggaran secara berkala.<br />\r\n<br />\r\n&quot;Melakukan revitalisasi penguatan UU dan kelembagaan dan sekaligus juga anggaran dan pengawasan, itu yang harus dilakukan terus menerus,&quot; tegasnya.</p>\r\n', 'http://localhost/p2bj/public/source/dua-tahun-jokowi-jk-dpr-sebut-kesenjangan-sosial-masih-tinggi.png', 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 04:34:42'),
(7, 'Polemik Sri Mulyani gantikan Rini Soemarno di Komisi VI DPR', '2016-10-17', '<p><strong>Merdeka.com - </strong>Menteri Keuangan, Sri Mulyani Indrawati menghadiri Rapat Kerja dengan komisi VI DPR RI untuk membahas rencana kerja dan anggaran kementerian negara/lembaga (RKAKL). Sri Mulyani hadir mewakili Menteri BUMN, Rini Soemarno.</p>\r\n\r\n<p>Kehadiran Ani di Komisi VI DPR menimbulkan polemik, terutama pembahasan soal Penyertaan Modal Negara (PMN) untuk BUMN. Ani kemudian menjelaskan dia tidak ingin membuat kegaduhan antara Komisi VI dengan komisi XI yang membawahi keuangan negara. Informasi saja, Komisi VI DPR selama ini membawahi BUMN, dan Komisi XI membawahi soal keuangan negara, belanja negara dan sebagainya.</p>\r\n\r\n<p>&quot;Kami tidak ada sedikit pun, selaku Menteri Keuangan dan mewakili Menteri BUMN menciptakan suasana yang dipersepsikan menimbulkan kerisauan di Komisi VI,&quot; ujar dia di komplek DPR RI, <strong><a href="http://www.merdeka.com/tag/j/jakarta/">Jakarta</a></strong>, Kamis (17/10).</p>\r\n\r\n<p>Menurut dia, kehadirannya bersama pejabat eselon I Kementerian BUMN adalah dalam rangka memenuhi panggilan komisi untuk pembahasan PMN yang sejatinya harus dilakukan oleh Rini Soemarno. Dia tidak menyangka jika keputusan tersebut malah menimbulkan polemik.</p>\r\n\r\n<p>&quot;Kami bukannya ada keinginan untuk adu domba antar komisi. Bagi kami, pemerintah menghormati dewan. Saya tidak ada keinginan, niat, atau berencana membuat suasana Komisi VI dan Komisi XI mengalami ketegangan,&quot; tuturnya.</p>\r\n\r\n<p>Untuk itu, mantan Direktur Pelaksana Bank Dunia tersebut akan melakukan koordinasi dengan Rini selaku Menteri BUMN. Sebab, secara struktural Rini adalah sosok yang bertanggung jawab penuh dalam segala hal di Kementerian BUMN.</p>\r\n\r\n<p>&quot;Kami akan konsultasi dengan Menteri BUMN sendiri apa yang dilakukan dan apa yang tidak dilakukan. Karena dalam struktur, BUMN punya menteri yang bertanggung jawab,&quot; tandas dia.</p>\r\n', 'http://localhost/p2bj/public/source/polemik-sri-mulyani-gantikan-rini-soemarno-di-komisi-vi-dpr.jpg', 'standart', 'info', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(8, 'Per Agustus, utang luar negeri Indonesia turun jadi Rp 4.203 tri', '2016-08-17', '<p>Bank Indonesia (BI) melansir data terbaru mengenai posisi utang luar negeri Indonesia. Per Agustus 2016, utang luar negeri Indonesia tercatat sebesar USD 323,05 miliar atau setara dengan Rp 4.203 triliun (kurs hari ini). Angka utang ini turun dibanding bulan sebelumnya atau Juli 2016 yang tercatat USD 324,67 miliar.</p>\r\n\r\n<p>Namun demikian, posisi utang per Agustus 2016 ini naik jika dibanding posisi akhir tahun lalu yang hanya USD 310,26 miliar.</p>\r\n\r\n<p>Dikutip dari data resmi Bank Indonesia, utang luar negeri Indonesia sebesar USD 323,05 miliar ini terdiri dari utang luar negeri pemerintah bersama Bank Indonesia serta swasta.</p>\r\n\r\n<p>Porsi utang luar negeri pemerintah sendiri mencapai USD 155,18 miliar dan Bank Indonesia sebesar USD 4,53 miliar. Total utang keduanya adalah USD 159,72 miliar. Total utang ini naik dari bulan sebelumnya yang hanya USD 159,70 miliar.</p>\r\n\r\n<p>Sedangkan porsi utang swasta tercatat sebesar USD 163,32 miliar. Angka utang ini turun dari bulan sebelumnya yang mencapai USD 164,97 miliar.</p>\r\n\r\n<p>Utang luar negeri swasta juga terbagi menjadi utang perbankan dan utang non-perbankan. Utang perbankan tercatat mencapai USD 28,64 miliar. Sedangkan utang luar negeri non-perbankan tercatat USD 134,68 miliar.</p>\r\n\r\n<p>Untuk non-perbankan, terbagi menjadi utang lembaga keuangan bukan bank atau nonbank financial corporation yang mencapai USD 10,77 miliar. Kemudian utang perusahaan bukan lembaga keuangan atau nonfinancial corporation sebesar USD 123,90 miliar.</p>\r\n', 'http://localhost/p2bj/public/source/per-agustus-utang-luar-negeri-indonesia-turun-jadi-rp-4203-triliun.jpg', 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', 'developer', '2016-10-20 05:36:44'),
(9, 'Kemandirian ekonomi tak terwujud di 2 tahun kepemimpinan Jokowi-', '2016-09-06', '<p>Institute for Development Economy and Finance (Indef) mencatat dua tahun Presiden <strong><strong><a href="http://profil.merdeka.com/indonesia/j/joko-widodo/berita/">Joko Widodo</a></strong></strong> dan Wakil Presiden <strong><strong><a href="http://profil.merdeka.com/indonesia/m/muhammad-jusuf-kalla/berita/">Jusuf Kalla</a></strong></strong> memimpin Indonesia belum mampu membangun kemandirian ekonomi. Salah satu indikatornya ialah masih bergantungnya Indonesia pada pangan impor.</p>\r\n\r\n<p>Ekonom Indef, Ahmad Heri Firdaus, mengatakan saat ini impor beras Indonesia masih sangat tinggi. &quot;Beras Januari-Desember tahun lalu USD 351 juta, nah ini Januari-Juli saja baru setengah tahun sudah USD 447 juta. Ini sampai Desember bisa naik sampai 200 persen,&quot; katanya saat ditemui di Kantornya, <strong><a href="http://www.merdeka.com/tag/j/jakarta/">Jakarta</a></strong>, Kamis (20/10).<br />\r\n<br />\r\nSelain beras, impor gandum tahun ini juga sudah melewati pencapaian tahun lalu. &quot;Kemudian gandum dari Januari-Juli sudah naik berapa persen dari tahun lalu. Sampai akhir tahun bisa naik sampai 50 persen,&quot; tambahnya.<br />\r\n<br />\r\nMenurutnya, dalam survei yang dilakukan Global Food Security Index, posisi Indonesia kalah dibanding negara tetangga seperti Vietnam dan Malaysia. Posisi Indonesia pada tahun ini menempati peringkat 71.<br />\r\n<br />\r\n&quot;Kita negara agraris, negara sumber daya alam, namun kita kalah sama Vietnam di 57, Argentina 37, Malaysia 35. Memang ada perbaikan namun kita hanya naik 3 sampai 4 poin dari tahun lalu,&quot; tutupnya.</p>\r\n', 'http://localhost/p2bj/public/source/kemandirian-ekonomi-tak-terwujud-di-2-tahun-kepemimpinan-jokowi-jk.jpg', 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(10, '2 Tahun Jokowi, DPR sorot proyek 35.000 MW dan lifting minyak RI', '2016-02-10', '<p>Ketua Komisi VII Dewan Perwakilan Rakyat (DPR), Gus Irawan ikut menilai kinerja pemerintahan Jokowi-JK yang telah berjalan dua tahun, tepat hari ini Kamis (10/02).</p>\r\n\r\n<p>Menurutnya, setelah 2 tahun kepemimpinan Presiden Jokowi, Pekerjaan Rumah (PR) yang masih harus diselesaikan adalah program listrik 35.000 megawatt (MW). Mega proyek ini dalam 2 tahun terakhir tidak mengalami perkembangan yang cukup baik.</p>\r\n\r\n<p>Menteri Koordinator Bidang Kemaritiman Luhut Binsar Panjaitan menyebut jika proyek 35.000 MW hanya akan terealisasi sebesar 25.000 MW saja. Sementara 10.000 MW masih under construction.</p>\r\n\r\n<p>&quot;Tentu saya kira capaiannya belum terlalu baik sampai saat ini,&quot; ujarnya saat ditemui di ruang rapat komisi VII, Komplek DPR, Senayan, <strong><a href="http://www.merdeka.com/tag/j/jakarta/">Jakarta</a></strong>, Kamis (10/02).</p>\r\n\r\n<p>Lanjutnya, selain mega proyek 35.000 MW, eksplorasi minyak juga masih menjadi PR bagi pemerintah untuk diselesaikan. Sebab, di tengah semakin merosotnya lifting minyak, jumlah konsumsi Bahan Bakar Minyak (BBM) justru terus meningkat.</p>\r\n\r\n<p>Tercatat, lifting minyak Indonesia saat ini mencapai 800.000 ribu barel per hari (BOPD). Sementara kebutuhannya sudah mencapai 1,4 juta BOPD. Artinya, masih ada gap sebesar 600.000 ribu BOPD yang harus dipenuhi pemerintah melalui impor minyak.</p>\r\n\r\n<p>&quot;Kan lifting minyak kita turun terus, itu juga satu tantangan kita juga sih, saya kira itu, lifting turun terus tapi produksi tak kunjung membaik. Kondisi yang produksi minyak kita terus menurun, menyebabkan impor yang lebih besar, impor yang besar itu menguras devisa negara, mengganggu stabilitas rupiah dan seterusnya itu menjadi penting,&quot; jelas dia.</p>\r\n\r\n<p>Meski demikian, dia tetap menilai kinerja pemerintah banyak yang perlu di apresiasi di tengah PR yang tak kunjung terselesaikan. Salah satunya upaya untuk menerapkan keadilan pada harga BBM di seluruh wilayah Indonesia.</p>\r\n\r\n<p>&quot;Soal misalnya juga satu harga BBM itu sebenarnya perintah konstitusi memang sesungguhnya. Terkait itu meskipun perintah konstitusi, kita tetap saja memberikan apresiasi kepada pemerintah, cuma yang penting, sekarang bahwa sekarang jangan hanya dipublish tapi, pelaksanaannya, entah kapan-kapan. Ini yang perlu kita kawal, kalau ukuran kinerja, target pembangunan sesungguhnya, tapi menyeluruh,&quot; pungkasnya.</p>\r\n', 'http://localhost/p2bj/public/source/2-tahun-jokowi-dpr-sorot-proyek-35000-mw-dan-lifting-minyak-ri.jpg', 'standart', 'news', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(11, 'Tiap tahun, aduan kasus perumahan meningkat', '2016-06-15', '<p>Pemerintah terus mengejar target pembangunan sejuta rumah per tahun. Ini untuk memangkas backlog atau kekurangan pasokan rumah di Indonesia dari sebesar 11,4 juta pada tahun lalu menjadi 6,8 juta unit pada 2019.</p>\r\n\r\n<p>Rumah subsidi menempati porsi terbesar dari target sejuta hunian tersebut. Tahun ini, pemerintah menargetkan pembangunan 700 ribu rumah subsidi.</p>\r\n\r\n<p>Mengingat, minat masyarakat yang ingin mendapatkan rumah murah tersebut sangat besar. Celakanya, kesempatan ini dimanfaatkan oleh oknum pengembang nakal, semacam PT Pratama Mega Konstruksindo. Sebanyak 16 orang tertipu pengembang rumah subsidi Grand Mutiara, Nanggerang, Bojong Gede, Bogor, Jawa Barat, tersebut.</p>\r\n\r\n<p>Pratama Mega Konstruksindo menjanjikan pembangunan rumah selesai dalam enam bulan. Itu terhitang sejak konsumen membayar uang muka. Faktanya, rumah tak kunjung nyata dan pengembang lenyap entah kemana.</p>\r\n\r\n<p>&quot;Nasib saya masih terkatung-katung karena pengembang hilang dan tidak dapat dihubungi,&quot; kata Fikri Faqih, salah seorang konsumen Pratama Mega, kepada merdeka.com, kemarin.</p>\r\n\r\n<p>&quot;Saya sudah memenuhi kewajiban saya untuk menyerahkan berkas, uang muka serta booking fee. Setidaknya sudah lebih dari Rp 30 juta saya keluarkan.&quot;</p>\r\n\r\n<p>Sejauh ini , pekerja media tersebut belum mengambil tindakan menuntut ganti rugi ke pengembang. Dia bersama belasan korban lain baru sebatas melakukan konsolidasi.</p>\r\n\r\n<p>Tulus Abadi, pengurus harian Yayasan Lembaga Konsumen Indonesia (YLKI), mendorong pihak yang dirugikan untuk membuat pengaduan. Saat ini, menurutnya, kasus pengembang yang melarikan uang konsumen sudah jarang terjadi.</p>\r\n\r\n<p>&quot;Dulu iya, sekitar tujuh tahun lalu ada pengembang nakal,&quot; katanya saat dihubungi.</p>\r\n\r\n<p>&quot;Kami pernah memfasilitasi kasus perumahan di Sentul, pengembang dikenakan pidana.&quot;</p>\r\n\r\n<p>Sepanjang tahun ini, menurut Tulus, pihaknya belum menerima satu pun pengaduan kasus perumahan. Namun, berdasarkan data lembaga berusia 43 tahun tersebut, pengaduan terkait properti meningkat setiap tahun.</p>\r\n\r\n<p>Sekedar ilustrasi, pada 2014, YLKI menerima pengaduan 1192 kasus. sebanyak 70 kasus diantaranya terkait perumahan. Ketiga terbesar setelah telekomunikasi atau multimedia (71 kasus) dan perbankan (115) kasus.</p>\r\n\r\n<p>Setahun kemudian, YLKI menerima pengaduan sebanyak 1030 kasus. Sebanyak 160 kasus (15,53 persen) terkait perumahan. Ini menggeser telekomunikasi atau multimedia yang hanya sebanyak 83 kasus (8,06 persen). Sementara, perbankan masih yang tertinggi. Yaitu, sebanyak 17 kasus (17,09 persen).</p>\r\n\r\n<p>Adapun kasus perumahan yang diadukan, umumnya, menyangkut pembangunan bermasalah. Ini meliputi kualitas bangunan, spesifikasi tidak sesuai dan lainnya.</p>\r\n\r\n<p>Kemudian persoalan terkait pengelolaan perumahan oleh pengembang. Lalu keterlambatan serah terima sertifikat dari pengembang kepada konsumen.</p>\r\n\r\n<p>Tulus memberikan sejumlah saran agar konsumen bisa terhindar dari pengembang nakal. Diantaranya, meneliti aspek legalitas pengembang, perizinan prinsip, dan status lahannya.</p>\r\n\r\n<p>&quot;Dilihat perizinan perumahan tingkat pusat dan dinas perumahan, cek prinsip dan tanahnya seperti apa.&quot;</p>\r\n', 'http://localhost/p2bj/public/source/aduan-kasus-perumahan-terus-meningkat-perumahan-rev-1.png', 'standart', 'info', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(12, 'Buka puasa bersama anggota P2BJ', '2016-06-29', '<p>Sebagai bagian dari gerakan #BekasJadiBerkah, P2BJ mengadakan buka puasa bersama 200 anak jalanan di Kantor Pos Kota Tua, Jakarta, Rabu (29/6/2016). Kegiatan ini dilakukan berkolaborasi dengan Yayasan Tri Kusuma Bangsa dan dihadiri seluruh karyawan P2BJIndonesia.<br />\r\n<br />\r\n&quot;Gerakan #BekasJadiBerkah mengajak seluruh masyarakat Indonesia dan juga karyawan P2BJ untuk bisa berbagi dengan mereka yang kurang beruntung. Di kegiatan ini, seluruh karyawan P2BJ berpartisipasi dengan menyumbangkan barang-barang bekas yang masih layak pakai,&quot; ujar Daniel Tumiwa,P2BJ.<br />\r\n<br />\r\nBarang bekas, menurut Daniel, bukan berarti rusak atau rongsokan. Secara fungsional barang bekas sudah tidak dibutuhkan lagi oleh pemiliknya, tetapi bisa sangat besar artinya bagi orang lain, contohnya anak-anak jalanan.</p>\r\n', 'http://localhost/p2bj/public/source/4591878498.jpg', 'standart', 'info', 'publish', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(13, 'Profil', '2016-10-20', '<h1 style="text-align: center;"><strong>Samsung kenalkan modul RAM 8GB baru, tercanggih dalam sejarah?</strong></h1>\r\n\r\n<p><strong>Merdeka.com - </strong>Insiden Galaxy Note 7 sepertinya tak membuat Samsung berhenti untuk berinovasi. Bukti terbarunya adalah memori RAM gadget mobile dengan kapasitas terbesar dalam sejarah.</p>\r\n\r\n<p>Dikutip dari <em>Phone Aren</em>a (20/10), Samsung baru saja memperkenalkan RAM berjenis LPDDR4 dengan kapasitas 8GB (gigabyte)! Perlu diketahui, saat ini RAM LPDDR4 kapasitas terbesarnya adalah 6GB seperti di OnePlus 3.</p>\r\n\r\n<p>RAM baru Samsung ini menggunakan empat cip memori LPDDR4 berkapasitas 16Gb (gigabit) yang dibangun dengan proses manufaktur 10 nanometer. Proses manufaktur ini adalah yang tercanggih saat ini karena menghasilkan cip dengan efisiensi terbaik.</p>\r\n\r\n<p>Lebih lanjut, RAM LPDDR4 8 GB Samsung mempunyai kecepatan pemrosesan data 4266 Mbps (megabit per second)! Artinya, kecepatan teknologi RAM ini dua kali lebih cepat dari RAM DDR4 di komputer dekstop. Meski demikian, energi yang dibutuhkan RAM ini masih sama dengan RAM LPDDR4 4GB lama Samsung yang dibangun dengan proses manufaktur 20 nanometer.</p>\r\n\r\n<p>Apabila RAM LPDDR4 8GB ini dipasang di smartphone kelas atas, diyakini performanya setara dengan komputer premium. Misalnya memainkan video berkualitas 4K atau menjalankan mesin virtual.</p>\r\n\r\n<p>Apakah Samsung Galaxy S8 akan memakai RAM ini? Tentu sangat menarik untuk ditunggu!</p>\r\n', 'http://localhost/p2bj/public/source/samsung-kenalkan-modul-ram-8gb-baru-tercanggih-dalam-sejarah.jpg', 'page', 'page', 'draft', 'developer', '2016-10-19 17:00:00', NULL, NULL),
(14, 'Mencoba Content', '2016-10-21', '<p>Mencoba Content</p>\r\n', 'http://localhost/p2bj/public/source/kemandirian-ekonomi-tak-terwujud-di-2-tahun-kepemimpinan-jokowi-jk.jpg', 'standart', 'news', 'publish', 'developer', '2016-10-20 17:00:00', 'developer', '2016-10-20 21:55:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_post_categories`
--

CREATE TABLE `cms_post_categories` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_post_categories`
--

INSERT INTO `cms_post_categories` (`id`, `post_id`, `category_id`) VALUES
(1, 1, 1),
(4, 3, 3),
(6, 6, 1),
(7, 7, 3),
(9, 2, 1),
(12, 9, 3),
(13, 8, 3),
(14, 8, 4),
(15, 10, 3),
(16, 10, 4),
(17, 11, 3),
(18, 14, 1),
(19, 14, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_post_images`
--

CREATE TABLE `cms_post_images` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `image_url` text,
  `caption` varchar(64) DEFAULT NULL,
  `link` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_post_images`
--

INSERT INTO `cms_post_images` (`id`, `post_id`, `image_url`, `caption`, `link`, `description`) VALUES
(13, 4, 'http://localhost/p2bj/public/source/3.jpg', 'Menteri Jonan akan wajibkan SPBU asing turut terapkan satu harga', '', '<p>Pemerintah Provinsi DKI <strong>Jakarta</strong> ternyata memenangkan banding reklamasi Pulau G yang diajukan Komite Nelayan Tradisional Indonesia (KNTI) di Pengadilan Tata Usaha Negara (PTUN). Walaupun sebelumnya, pihak penggugat juga sempat memenangkan pengaduannya.</p>\r\n'),
(14, 4, 'http://localhost/p2bj/public/source/2.jpg', '  Rano Karno ngaku belum tahu detil insiden penyerangan pospol C', '', '<p>Puluhan mahasiswa dan pelajar yang tergabung dalam Pengurus Daerah Kesatuan Aksi Mahasiswa Muslim Indonesia (PD KAMMI) Semarang dan Pengurus Wilayah Pelajar Islam Indonesia (PW PII) Jawa Tengah melakukan aksi unjuk rasa di Halaman Kantor DPRD Provinsi Jawa Tengah Jalan Pahlawan, Kota Semarang, Jawa Tengah Kamis</p>\r\n'),
(15, 4, 'http://localhost/p2bj/public/source/1.jpg', 'Pemprov DKI menangkan gugatan terkait reklamasi Pulau G di PTUN', '', '<p>Menteri Energi dan Sumber Daya Mineral (ESDM) <strong><strong>Ignasius Jonan</strong></strong> ingin pemberlakuan kebijakan Bahan Bakar Minyak (BBM) satu harga juga berlaku pada pengusaha BBM asing. Di Indonesia, terdapat sejumlah pengusaha BBM asing seperti Petronas, Total, hingga Shell.</p>\r\n'),
(22, 5, 'http://localhost/p2bj/public/source/180599_large.jpg', 'Anies-Sandiaga Luncurkan Salam Bersama', '', '<p>Bakal calon Gubernur DKI Jakarta Anies Baswedan (kiri) dan bakal calon Wakil Gubernur DKI Jakarta Sandiaga Uno meluncurkan Salam Bersama di Posko Cicurug, Menteng, Jakarta Pusat, Kamis (20/10/2016). Menurut Anies, Salam Bersama ini merupakan simbol keramahtamahan, kebersamaan, dan kekompakan.</p>\r\n'),
(23, 5, 'http://localhost/p2bj/public/source/180554_large.jpg', 'BNN Ungkap Penyelundupan 62,9 Kg Sabu dengan Pompa Air', '', '<p>Kepala Badan Narkotika Nasional (BNN) Komjen Pol Budi Waseso (kiri) berjabat tangan dengan Dirjen Bea dan Cukai Heru Pambudi (kanan) saat konferensi pers penyelundupan narkotika jenis sabu di kantor BNN, Jakarta, Kamis (20/10/2016). BNN dan Ditjen Bea dan Cukai kembali berhasil mengungkap jaringan sindikat narkotika internasional yang menyelundupkan 69,2 kg sabu asal Malaysia yang dimasukkan dalam lima unit mesin pompa air di kawasan Demak, Jawa Tengah dengan tersanga sebanyak lima orang yang terancam hukuman maksimal pidana mati.</p>\r\n'),
(24, 5, 'http://localhost/p2bj/public/source/180555_large.jpg', 'BNN Ungkap Penyelundupan 62,9 Kg Sabu dengan Pompa Air', '', '<p>Kepala Badan Narkotika Nasional (BNN) Komjen Pol Budi Waseso (kiri) berjabat tangan dengan Dirjen Bea dan Cukai Heru Pambudi (kanan) saat konferensi pers penyelundupan narkotika jenis sabu di kantor BNN, Jakarta, Kamis (20/10/2016). BNN dan Ditjen Bea dan Cukai kembali berhasil mengungkap jaringan sindikat narkotika internasional yang menyelundupkan 69,2 kg sabu asal Malaysia yang dimasukkan dalam lima unit mesin pompa air di kawasan Demak, Jawa Tengah dengan tersanga sebanyak lima orang yang terancam hukuman maksimal pidana mati.</p>\r\n'),
(25, 5, 'http://localhost/p2bj/public/source/180606_large.jpg', 'Potong Untaian Bunga, Hary Tanoe Resmikan Kantor DPW Partai Peri', '', '<p>Ketua Umum Partai Perindo Hary Tanoesoedijo melakukan pemotongan tumpeng pada acara peresmian Kantor DPW Partai Perindo Jawa Timur di Jalan Kertajaya, Surabaya .</p>\r\n'),
(26, 5, 'http://localhost/p2bj/public/source/180607_large.jpg', 'Potong Untaian Bunga, Hary Tanoe Resmikan Kantor DPW Partai Peri', '', '<p>Ketua Umum Partai Perindo Hary Tanoesoedijo melakukan pemotongan tumpeng pada acara peresmian Kantor DPW Partai Perindo Jawa Timur di Jalan Kertajaya, Surabaya</p>\r\n'),
(27, 5, 'http://localhost/p2bj/public/source/180608_large.jpg', 'Potong Untaian Bunga, Hary Tanoe Resmikan Kantor DPW Partai Peri', '', '<p>Ketua Umum Partai Perindo Hary Tanoesoedijo melakukan pemotongan tumpeng pada acara peresmian Kantor DPW Partai Perindo Jawa Timur di Jalan Kertajaya, Surabaya</p>\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_post_options`
--

CREATE TABLE `cms_post_options` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `key` varchar(64) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_post_tags`
--

CREATE TABLE `cms_post_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_post_tags`
--

INSERT INTO `cms_post_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 14, 1),
(3, 14, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_slugs`
--

CREATE TABLE `cms_slugs` (
  `id` int(11) NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `slug` varchar(64) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_slugs`
--

INSERT INTO `cms_slugs` (`id`, `type`, `slug`, `relation_id`) VALUES
(1, 'category', 'politik', 1),
(2, 'category', 'partai', 2),
(3, 'category', 'ekonomi', 3),
(4, 'category', 'bisnis', 4),
(5, 'tag', 'pilgub-dki', 1),
(6, 'tag', 'ahok', 2),
(7, 'post', 'pilihan-gubernur-dki', 1),
(8, 'category', 'ukm', 5),
(9, 'post', 'jokowi-ajak-gubernur-se-indonesia-cari-solusi-berantas-pungli-di', 2),
(10, 'post', 'bank-indonesia-resmi-turunkan-suku-bunga-acuan-jadi-4-75-persen', 3),
(11, 'post', 'main-slider', 4),
(12, 'post', 'main-gallery', 5),
(13, 'post', 'dua-tahun-jokowi-jk-dpr-sebut-kesenjangan-sosial-masih-tinggi', 6),
(14, 'post', 'polemik-sri-mulyani-gantikan-rini-soemarno-di-komisi-vi-dpr', 7),
(15, 'post', 'per-agustus-utang-luar-negeri-indonesia-turun-jadi-rp-4-203-tril', 8),
(16, 'post', 'kemandirian-ekonomi-tak-terwujud-di-2-tahun-kepemimpinan-jokowi-', 9),
(17, 'post', '2-tahun-jokowi-dpr-sorot-proyek-35-000-mw-dan-lifting-minyak-ri', 10),
(18, 'category', 'test', 6),
(19, 'post', 'tiap-tahun-aduan-kasus-perumahan-meningkat', 11),
(20, 'post', 'buka-puasa-bersama-anggota-p2bj', 12),
(21, 'tag', 'testa', 3),
(22, 'post', 'profil', 13),
(23, 'category', 'testaa', 7),
(24, 'category', 'testaaa', 8),
(25, 'category', 'asdadasd', 9),
(26, 'category', 'asdasdsad', 10),
(27, 'category', 'asdaaa', 11),
(28, 'category', 'aaaaa', 12),
(29, 'category', 'aaaaaa', 13),
(30, 'category', 'aasdasdasd', 14),
(31, 'post', 'mencoba-content', 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cms_tags`
--

CREATE TABLE `cms_tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(64) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cms_tags`
--

INSERT INTO `cms_tags` (`id`, `tag`, `description`) VALUES
(1, 'Pilgub DKI', NULL),
(2, 'Ahok', NULL),
(3, 'testa', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_access`
--

CREATE TABLE `usr_access` (
  `id` int(11) NOT NULL,
  `access` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_access_key`
--

CREATE TABLE `usr_access_key` (
  `id` int(11) NOT NULL,
  `access_id` int(11) DEFAULT NULL,
  `access_key` varchar(16) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_groups`
--

CREATE TABLE `usr_groups` (
  `id` int(11) NOT NULL,
  `group` varchar(64) DEFAULT NULL,
  `description` text,
  `created_by` varchar(64) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usr_groups`
--

INSERT INTO `usr_groups` (`id`, `group`, `description`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Developer', 'For All Developer', 'developer', 2016, 'developer', 2016);

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_group_access`
--

CREATE TABLE `usr_group_access` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `access_key` varchar(32) DEFAULT NULL,
  `permission` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_login_logs`
--

CREATE TABLE `usr_login_logs` (
  `id` int(11) NOT NULL,
  `login_date` date DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `ip_address` varchar(32) DEFAULT NULL,
  `user_agent` text,
  `attemp` int(11) DEFAULT NULL,
  `success` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usr_login_logs`
--

INSERT INTO `usr_login_logs` (`id`, `login_date`, `username`, `ip_address`, `user_agent`, `attemp`, `success`, `created_at`, `updated_at`) VALUES
(1, '2016-10-20', 'developer', '192.168.1.99', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 01:14:53', NULL),
(2, '2016-10-20', 'developer', '192.168.1.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 01:27:59', NULL),
(3, '2016-10-20', 'developer', '192.168.1.3', 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 03:30:07', NULL),
(4, '2016-10-20', 'developer', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 04:29:48', NULL),
(5, '2016-10-20', 'developer', '192.168.1.99', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 05:31:46', NULL),
(6, '2016-10-20', 'admin', '192.168.1.22', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 0, '2016-10-20 06:30:03', NULL),
(7, '2016-10-20', 'cms', '192.168.1.22', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 0, '2016-10-20 06:30:33', NULL),
(8, '2016-10-20', 'developer', '192.168.1.22', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 06:30:40', NULL),
(9, '2016-10-20', 'developer', '192.168.1.99', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 06:44:01', NULL),
(10, '2016-10-21', 'developer', '192.168.1.99', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 1, 1, '2016-10-20 21:29:02', NULL),
(11, '2016-10-21', 'developer', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', 2, 1, '2016-10-21 04:16:52', '2016-10-21 04:16:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usr_users`
--

CREATE TABLE `usr_users` (
  `id` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_name` varchar(64) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_by` varchar(64) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usr_users`
--

INSERT INTO `usr_users` (`id`, `username`, `email`, `password`, `profile_name`, `group_id`, `last_login`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'developer', 'uone.developer@gmail.com', '3dacbce532ccd48f27fa62e993067b3c35f094f7', 'Developer', 6, '2016-10-21 04:16:58', 'system', NULL, 'developer', '2016-10-14 09:13:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `cms_categories`
--
ALTER TABLE `cms_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_comments`
--
ALTER TABLE `cms_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_options`
--
ALTER TABLE `cms_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_posts`
--
ALTER TABLE `cms_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_post_categories`
--
ALTER TABLE `cms_post_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_post_images`
--
ALTER TABLE `cms_post_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_post_options`
--
ALTER TABLE `cms_post_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_post_tags`
--
ALTER TABLE `cms_post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_slugs`
--
ALTER TABLE `cms_slugs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_tags`
--
ALTER TABLE `cms_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_access`
--
ALTER TABLE `usr_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_access_key`
--
ALTER TABLE `usr_access_key`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_groups`
--
ALTER TABLE `usr_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_group_access`
--
ALTER TABLE `usr_group_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_login_logs`
--
ALTER TABLE `usr_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr_users`
--
ALTER TABLE `usr_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_categories`
--
ALTER TABLE `cms_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cms_comments`
--
ALTER TABLE `cms_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_options`
--
ALTER TABLE `cms_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_posts`
--
ALTER TABLE `cms_posts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `cms_post_categories`
--
ALTER TABLE `cms_post_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cms_post_images`
--
ALTER TABLE `cms_post_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `cms_post_options`
--
ALTER TABLE `cms_post_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_post_tags`
--
ALTER TABLE `cms_post_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms_slugs`
--
ALTER TABLE `cms_slugs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `cms_tags`
--
ALTER TABLE `cms_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usr_access`
--
ALTER TABLE `usr_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usr_access_key`
--
ALTER TABLE `usr_access_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usr_groups`
--
ALTER TABLE `usr_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `usr_group_access`
--
ALTER TABLE `usr_group_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usr_login_logs`
--
ALTER TABLE `usr_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `usr_users`
--
ALTER TABLE `usr_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
