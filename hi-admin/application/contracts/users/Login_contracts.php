<?php

class Login_contracts extends Contracts {

	public function __construct() {
		parent::__construct();
	}

	public function login() {
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('username', 'Username', 'required');
		$this->contracts->form_validation->set_rules('password', 'Password', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withValidation()->back();
		}
	}

	public function change_profile() {
		$this->contracts->load->library('form_validation');		
		$this->contracts->form_validation->set_rules('profile_name', 'Nama', 'required');
		$this->contracts->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

	public function change_password() {
		$this->contracts->load->model('users/users_m');
		$result = $this->contracts->users_m->find_by('username', getLogin('username'));
		if ($result->password <> sha1($this->contracts->input->post('old_password'))) {
			$this->contracts->redirect->with('errorMessage', 'Password lama tidak sesuai.')->back();
		}
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('old_password', 'Password Lama', 'required');
		$this->contracts->form_validation->set_rules('new_password', 'Password Baru', 'required');
		$this->contracts->form_validation->set_rules('confirm_new_password', 'Ulangi Password Baru', 'required|matches[new_password]');
		if(!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withValidation()->back();
		}
	}

}