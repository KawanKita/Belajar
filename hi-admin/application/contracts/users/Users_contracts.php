<?php

class Users_contracts extends Contracts {

	public function store() {
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->contracts->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');
		$this->contracts->form_validation->set_rules('confirm_password', 'Ulangi Password', 'matches[password]');
		$this->contracts->form_validation->set_rules('profile_name', 'Nama', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}		

	public function edit($id) {
		$this->contracts->load->model('users/users_m');
		$result = $this->contracts->users_m->find($id);
		if ($result->username == getLogin('username')) {
			$this->contracts->redirect->with('errorMessage', 'Tidak diperbolehkan melakukan edit pada akun sendiri.')->back();
		}
	}

	public function update_profile() {		
		$this->contracts->load->library('form_validation');		
		$this->contracts->form_validation->set_rules('profile_name', 'Nama', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}	
	}

	public function update_setting() {		
		$this->contracts->load->library('form_validation');		
		$this->contracts->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}	
	}

	public function change_password() {
		$this->contracts->load->library('form_validation');		
		$this->contracts->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');
		$this->contracts->form_validation->set_rules('confirm_password', 'Ulangi Password', 'required|matches[password]');		
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

}