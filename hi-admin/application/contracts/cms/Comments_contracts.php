<?php

class Comments_contracts extends Contracts {

	public function __construct() {
		parent::__construct();
	}

	public function reply() {
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('comment', 'Komentar', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

}