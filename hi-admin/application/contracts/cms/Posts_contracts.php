<?php

class Posts_contracts extends Contracts {

	public function __construct() {
		parent::__construct();
	}

	public function store() {
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('title', 'Judul', 'required');
		$this->contracts->form_validation->set_rules('slug', 'Slug', 'required|is_unique[cms_slugs.slug]');
		$this->contracts->form_validation->set_rules('type', 'Jenis Post', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

	public function update($id) {
		$this->contracts->load->model('cms/posts_m');		
		$result = $this->contracts->posts_m->find_post_or_fail($id);
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('title', 'Judul', 'required');
		if ($result->slug <> $this->contracts->input->post('slug')) {
			$this->contracts->form_validation->set_rules('slug', 'Slug', 'required|is_unique[cms_slugs.slug]');
		}
		$this->contracts->form_validation->set_rules('type', 'Jenis Post', 'required');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

	public function delete($id) {
		$this->contracts->load->model('cms/posts_m');
		$this->contracts->posts_m->find_or_fail($id);			
	}

	public function publish($id) {
		$this->contracts->load->model('cms/posts_m');
		$this->contracts->posts_m->find_or_fail($id);
	}

	public function draft($id) {
		$this->contracts->load->model('cms/posts_m');
		$this->contracts->posts_m->find_or_fail($id);
	}

}