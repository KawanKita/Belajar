<?php

class Tags_contracts extends Contracts {

	public function store() {
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('tag', 'Tag', 'required');
		$this->contracts->form_validation->set_rules('slug', 'Slug', 'required|is_unique[cms_slugs.slug]');
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

	public function update($id) {
		$this->contracts->load->model('cms/tags_m');
		$result = $this->contracts->tags_m->find_tag_or_fail($id);
		$this->contracts->load->library('form_validation');
		$this->contracts->form_validation->set_rules('tag', 'Tag', 'required');
		if ($result->slug <> $this->contracts->input->post('slug')) {
			$this->contracts->form_validation->set_rules('slug', 'Slug', 'required|is_unique[cms_slugs.slug]');
		}		
		if (!$this->contracts->form_validation->run()) {
			$this->contracts->redirect->withInput()->withValidation()->back();
		}
	}

}