<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function locale_date($timestamp) {
	$timestamp = strtotime($timestamp);
	if ($time = $timestamp) {
		return date(getConfig('_date'), $time);
	} else {
		return null;
	}
}

function locale_time($timestamp) {	
	$timestamp = strtotime($timestamp);	
	if ($time = $timestamp) {
		return date(getConfig('_time'), $time);
	} else {
		return null;
	}
}

function locale_datetime($timestamp) {	
	$timestamp = strtotime($timestamp);	
	if ($time = $timestamp) {
		return date(getConfig('_datetime'), $time);
	} else {
		return null;
	}
}

function locale_number($number) {
	$number = number_format($number);
	$parse = explode($number, '.');
	$result = str_replace(',', getConfig('_thousand_separator'), $parse[0]);
	if (isset($parse[1])) {
		$result .= getConfig('_decimal_separator') . $parse[1];
	}
	return $result;
}

function locale_currency($str) {	
	return getConfig('_currency').locale_number($str);
}

function locale_human_date($timestamp, $format = null) {	
	$timestamp = strtotime($timestamp);
	if ($time = $timestamp) {
		$y = date('Y', $time);
		$m = getConfig('_month')[date('m', $time)];
		$d = date('d', $time);
		$H = date('H', $time);
		$i = date('i', $time);
		$s = date('s', $time);
		if (!$format) {
			$humanDate = getConfig('_human_date');
		} else {
			$humanDate = $format;
		}
		$humanDate = str_replace(array('{Y}', '{m}', '{d}'), array($y, $m, $d), $humanDate);
		return $humanDate;
	} else {
		return null;
	}
}

function locale_human_datetime($timestamp) {	
	$timestamp = strtotime($timestamp);
	if ($time = $timestamp) {
		$y = date('Y', $time);
		$m = getConfig('_month')[date('m', $time)];
		$d = date('d', $time);
		$H = date('H', $time);
		$i = date('i', $time);
		$s = date('s', $time);
		$humanDate = getConfig('_human_datetime');
		$humanDate = str_replace(array('{Y}', '{m}', '{d}', '{H}', '{i}', '{s}'), array($y, $m, $d, $H, $i, $s), $humanDate);
		return $humanDate;
	} else {
		return null;
	}
}