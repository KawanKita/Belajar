<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_m extends BaseModel {

	protected $table = 'cms_tags';
	protected $fillable = array('tag', 'description');
	protected $author = false;
	protected $timestamp = false;

	public function get_tags() {
		return $this->db->select('cms_tags.*, cms_slugs.slug')
		->join('cms_slugs', 'cms_slugs.relation_id = cms_tags.id and type="tag"')
		->get('cms_tags')
		->result();
	}

	public function find_tag($id) {
		return $this->db->select('cms_tags.*, cms_slugs.slug')
		->join('cms_slugs', 'cms_slugs.relation_id = cms_tags.id and type="tag"')
		->where('cms_tags.id', $id)
		->get('cms_tags')
		->row();
	}

	public function find_tag_or_fail($id) {
		$result = $this->find_tag($id);
		if ($result) {
			return $result;
		} else {
			$this->model_exceptions->error('find_or_fail');
		}
	}
	

	public function insert_tag($record) {
		$this->insert($this->fillable($record));
		$category_id = $this->insert_id();
		$this->db->insert('cms_slugs', array(
			'type' => 'tag',
			'slug' => $record['slug'],
			'relation_id' => $category_id
		));
		return true;
	}

	public function update_tag($id, $record) {
		$this->update($id, $this->fillable($record));
		$category_id = $this->insert_id();
		$this->db->where('relation_id', $id)->where('type', 'tag')->update('cms_slugs', array(			
			'slug' => $record['slug']			
		));
		return true;
	}

	public function delete_category($id) {
		$this->db->where('relation_id', $id)->where('type', 'tag')->delete('cms_slugs');
		$this->db->delete($id);
		return true;
	}	
	
}