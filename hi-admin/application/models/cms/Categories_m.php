<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_m extends BaseModel {

	protected $table = 'cms_categories';
	protected $fillable = array('category', 'description', 'parent_id');
	protected $tree = true;
	protected $author = false;
	protected $timestamp = false;

	public function get_categories() {
		return $this->db->select('cms_categories.*, cms_slugs.slug')
		->join('cms_slugs', 'cms_slugs.relation_id = cms_categories.id and type="category"')
		->get('cms_categories')
		->result();
	}

	public function find_category($id) {
		return $this->db->select('cms_categories.*, cms_slugs.slug')
		->join('cms_slugs', 'cms_slugs.relation_id = cms_categories.id and type="category"')
		->where('cms_categories.id', $id)
		->get('cms_categories')
		->row();
	}

	public function find_category_or_fail($id) {
		$result = $this->find_category($id);
		if ($result) {
			return $result;
		} else {
			$this->model_exceptions->error('find_or_fail');
		}
	}
	

	public function insert_category($record) {
		$this->insert($this->fillable($record));
		$category_id = $this->insert_id();
		$this->db->insert('cms_slugs', array(
			'type' => 'category',
			'slug' => $record['slug'],
			'relation_id' => $category_id
		));
		return true;
	}

	public function update_category($id, $record) {
		$this->update($id, $this->fillable($record));
		$category_id = $this->insert_id();
		$this->db->where('relation_id', $id)->where('type', 'category')->update('cms_slugs', array(			
			'slug' => $record['slug']			
		));
		return true;
	}

	public function delete_category($id) {
		$this->db->where('relation_id', $id)->where('type', 'category')->delete('cms_slugs');
		$this->db->delete($id);
		return true;
	}	

}