<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends BaseController {

	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('cms/posts_m');
		$this->load->model('cms/comments_m');

		$drafts = $this->posts_m->get_posts('draft');
		$comments = $this->comments_m->get_comments('pengaduan', 'wappr');

		$this->load->view('dashboard', array(
			'drafts' => $drafts,
			'comments' => $comments
		));
	}

}
