<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('cms/posts_m');
	}

	public function index($status = null) {				
		$this->load->model('cms/categories_m');		
		$ls_categories = tree_data_lists($this->categories_m->get(), 'id', 'parent_id', '', 'tree_id', 'category', 'Semua Kategori');		
		$this->load->model('cms/tags_m');
		$ls_tags = lists($this->tags_m->get(), 'id', 'tag', 'Semua Tag');
		$rs_archive = $this->posts_m->get_archive();
		$ls_archive[''] = 'Semua Arsip';		
		foreach($rs_archive as $archive) {
			$ls_archive[$archive->archive] = locale_human_date($archive->archive, '{m} {Y}');
		}
		$rs_status = $this->posts_m->get_status();
		$result = $this->posts_m->get_posts($status, $this->input->get('type'), $this->input->get('archive'), $this->input->get('category'), $this->input->get('tag'));
		foreach($result as $key => $row) {
			$result[$key]->categories = $this->posts_m->get_categories($row->id);
			$result[$key]->tags = $this->posts_m->get_tags($row->id);
		} 
		$this->load->view('cms/posts/index', array(			
			'ls_archive' => $ls_archive,
			'ls_categories' => $ls_categories,
			'ls_tags' => $ls_tags,
			'rs_status' => $rs_status,
			'result' => $result
		));
	}

	public function create() {
		$format = $this->input->get('format');
		switch ($format) {
			case 'standart':
				$this->create_post();
				break;			
			case 'page':
				$this->create_page();
				break;
			case 'image':
				$this->create_image();
				break;
			default:
				$this->create_post();
				break;
		}
	}

	public function store() {
		$post = $this->input->post();
		$result = $this->posts_m->insert_post($post);
		if ($result) {
			$this->redirect->with('successMessage', 'Post berhasil disimpan.')->to(routes()->name('cms_posts'));
		} else {
			$this->redirect->with('errorMessage', 'Post gagal disimpan.')->back();
		}
	}

	public function edit($id) {
		$result = $this->posts_m->find_post_or_fail($id);
		$format = $this->input->get('format');
		if (!$format) {
			$format = $result->format;		
		}
		switch ($format) {
			case 'standart':
				$this->edit_post($result);
				break;	
			case 'page':
				$this->edit_page($result);
				break;
			case 'image':
				$this->edit_image($result);
				break;
			default:
				$this->edit_post($result);
				break;
		}
	}

	public function update($id) {
		$post = $this->input->post();
		$result = $this->posts_m->update_post($id, $post);
		if ($result) {
			$this->redirect->with('successMessage', 'Post berhasil diperbarui.')->to(routes()->name('cms_posts'));
		} else {
			$this->redirect->with('errorMessage', 'Post gagal diperbarui.')->back();
		}
	}

	public function delete($id) {
		$result = $this->posts_m->delete_post($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Post berhasil dihapus.')->to(routes()->name('cms_posts'));
		} else {
			$this->redirect->with('errorMessage', 'Post gagal dihapus.')->back();
		}
	}

	public function publish($id) {
		$result = $this->posts_m->publish($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Post berhasil diterbitkan.')->to(routes()->name('cms_posts'));
		} else {
			$this->redirect->with('errorMessage', 'Post gagal diterbitkan.')->back();
		}
	}

	public function draft($id) {
		$result = $this->posts_m->draft($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Post berhasil diubah menjadi draft.')->to(routes()->name('cms_posts'));
		} else {
			$this->redirect->with('errorMessage', 'Post gagal diubah menjadi draft.')->back();
		}
	}

	protected function create_post() {		
		$this->load->model('cms/categories_m');
		$rs_categories = tree_data($this->categories_m->get(), 'id', 'parent_id');		
		$this->load->model('cms/tags_m');
		$ls_tags = lists($this->tags_m->get(), 'id', 'tag');
		$this->load->view('cms/posts/standart/create', array(
			'rs_categories' => $rs_categories,
			'ls_tags' => $ls_tags
		));
	}

	protected function edit_post($result) {		
		$this->load->model('cms/categories_m');
		$rs_categories = tree_data($this->categories_m->get(), 'id', 'parent_id');		
		$this->load->model('cms/tags_m');
		$ls_tags = lists($this->tags_m->get(), 'id', 'tag');		
		$result->categories = new stdClass();
		foreach($this->posts_m->get_categories($result->id) as $category) {			
			$result->categories->{$category->id} = $category->id;
		}
		foreach($this->posts_m->get_tags($result->id) as $tag) {
			$result->tags[] = $tag->id;
		}
		$this->load->view('cms/posts/standart/edit', array(
			'rs_categories' => $rs_categories,
			'ls_tags' => $ls_tags,
			'result' => $result
		));
	}

	protected function create_page() {		
		$this->load->view('cms/posts/page/create');
	}

	protected function edit_page($result) {		
		$this->load->view('cms/posts/page/edit', array(			
			'result' => $result
		));
	}

	public function create_image() {		
		$this->load->model('cms/categories_m');
		$rs_categories = tree_data($this->categories_m->get(), 'id', 'parent_id');		
		$this->load->model('cms/tags_m');
		$ls_tags = lists($this->tags_m->get(), 'id', 'tag');
		$this->load->view('cms/posts/image/create', array(
			'rs_categories' => $rs_categories,
			'ls_tags' => $ls_tags
		));
	}

	protected function edit_image($result) {		
		$this->load->model('cms/categories_m');
		$rs_categories = tree_data($this->categories_m->get(), 'id', 'parent_id');		
		$this->load->model('cms/tags_m');
		$ls_tags = lists($this->tags_m->get(), 'id', 'tag');		
		$result->categories = new stdClass();
		foreach($this->posts_m->get_categories($result->id) as $category) {			
			$result->categories->{$category->id} = $category->id;
		}
		foreach($this->posts_m->get_tags($result->id) as $tag) {
			$result->tags[] = $tag->id;
		}
		foreach($this->posts_m->get_images($result->id) as $image) {
			$result->images[] = (array) $image;
		}
		$this->load->view('cms/posts/image/edit', array(
			'rs_categories' => $rs_categories,
			'ls_tags' => $ls_tags,
			'result' => $result
		));
	}

}
