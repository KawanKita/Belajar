<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('cms/comments_m');
	}

	public function index($status = null) {
		$rs_status = $this->comments_m->get_status();
		$result = $this->comments_m->get_comments($this->input->get('type'), $status);
		$this->load->view('cms/comments/index', array(
			'rs_status' => $rs_status,
			'result' => $result
		));
	}

	public function reply($id) {
		$this->comments_m->find_or_fail($id);
		$post = $this->input->post();
		$result = $this->comments_m->reply($id, $post);
		if ($result) {
			$this->redirect->with('successMessage', 'Komentar berhasil dibalas.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Komentar gagal dibalas.')->back();
		}
	}

	public function delete($id) {
		$this->comments_m->find_or_fail($id);
		$result = $this->comments_m->delete_comment($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Komentar berhasil dihapus.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Komentar gagal dihapus.')->back();
		}
	}

	public function appr($id) {
		$this->comments_m->find_or_fail($id);
		$result = $this->comments_m->appr($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Komentar berhasil disetujui.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Komentar gagal disetujui.')->back();
		}
	}

	public function wappr($id) {
		$this->comments_m->find_or_fail($id);
		$result = $this->comments_m->wappr($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Komentar berhasil dibatalkan dari persetujuan.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Komentar gagal dibatalkan dari persetujuan.')->back();
		}
	}

}
