<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('cms/categories_m');
	}

	public function index() {
		$categories = $this->categories_m->get_categories();
		$ls_categories = tree_data_lists($categories, 'id', 'parent_id', '', 'id', 'category', 'Sebagai Induk');
		$result = tree_data($categories, 'id', 'parent_id');		
		$this->load->view('cms/categories/index', array(
			'result' => $result,
			'ls_categories' => $ls_categories	
		));
	}

	public function store() {
		$post = $this->input->post();
		$result = $this->categories_m->insert_category($post);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil ditambahkan.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal ditambahkan.')->back();
		}
	}

	public function edit($id) {
		$category = $this->categories_m->find_category_or_fail($id);
		$categories = $this->categories_m->get_categories();
		$ls_categories = tree_data_lists($categories, 'id', 'parent_id', '', 'id', 'category', 'Sebagai Induk');
		$result = tree_data($categories, 'id', 'parent_id');		
		$this->load->view('cms/categories/edit', array(
			'result' => $result,
			'ls_categories' => $ls_categories,
			'category' => $category
		));
	}

	public function update($id) {
		$post = $this->input->post();
		$result = $this->categories_m->update_category($id, $post);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil diperbarui.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal diperbarui.')->back();
		}
	}

	public function delete($id) {
		$this->categories_m->find_or_fail($id);
		$result = $this->categories_m->delete($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil dihapus.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal dihapus.')->back();
		}
	}

}
