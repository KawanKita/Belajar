<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('cms/tags_m');
	}

	public function index() {
		$result = $this->tags_m->get_tags();		
		$this->load->view('cms/tags/index', array(
			'result' => $result
		));
	}

	public function store() {
		$post = $this->input->post();
		$result = $this->tags_m->insert_tag($post);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil ditambahkan.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal ditambahkan.')->back();
		}
	}

	public function edit($id) {
		$tag = $this->tags_m->find_tag_or_fail($id);		
		$result = $this->tags_m->get_tags();
		$this->load->view('cms/tags/edit', array(
			'result' => $result,			
			'tag' => $tag
		));
	}

	public function update($id) {
		$post = $this->input->post();
		$result = $this->tags_m->update_tag($id, $post);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil diperbarui.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal diperbarui.')->back();
		}
	}

	public function delete($id) {
		$this->tags_m->find_or_fail($id);
		$result = $this->tags_m->delete($id);
		if ($result) {
			$this->redirect->with('successMessage', 'Kategori berhasil dihapus.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Kategori gagal dihapus.')->back();
		}
	}

}
