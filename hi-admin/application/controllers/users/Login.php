<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('users/users_m');
	}

	public function index() {		
		$this->load->view('users/login/login');
	}	

	public function attemp() {
		$post = $this->input->post();	
		$result = $this->users_m->attemp($post['username'], $post['password']);		
		if (!$result) {
			$this->redirect->with('errorMessage', 'Username / Password tidak sesuai.')->back();
		}
		setLogin(array(
			'id' => $result->id,
			'username' => $result->username,
			'email' => $result->email,
			'group_id' => $result->group_id,
			'profile_name' => $result->profile_name,
			'login_at' => date('Y-m-d H:i:s'),

		));	
		$this->redirect->intended(routes()->name('dashboard'));
	}

	public function logout() {
		unsetLogin();
		$this->redirect->to(routes()->name('login'));
	}

	public function setting() {
		$this->load->view('users/login/setting');
	}

	public function change_profile() {
		$post = $this->input->post();				
		$result = $this->users_m->change_profile(getLogin('username'), $post);				
		if ($result) {
			$login = getLogin();			
			setLogin(array(
				'id' => $login['id'],
				'username' => $login['username'],
				'email' => $post['email'],
				'group_id' => $login['group_id'],
				'profile_name' => $post['profile_name'],
				'login_at' => $login['login_at']
			));
			$this->redirect->with('successMessage', 'Profile berhasil diperbaui.')->back();
		} else {
			$this->redirect->with('errorMessage', 'Profile gagal diperbaui.')->back();
		}
	}

	public function change_password() {
		$post = $this->input->post();
		$result = $this->users_m->change_password(getLogin('username'), $post['new_password']);
		if ($result) {
			$this->redirect->with('successMessage', 'Password berhasil diperbarui.');
		} else{				
			$this->redirect->with('errorMessage', 'Password gagal diperbarui.');
		}
		$this->redirect->back();		
	}

	

}
