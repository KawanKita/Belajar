<?php section('content') ?>
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Pengaturan</h3>
        </div>                    
        <div class="box-body box-spacing">
			<?= getview('layouts/partials/message') ?>
			<?= getview('layouts/partials/validation') ?>
			<?= $this->form->model(getLogin(), routes()->name('setting_change_profile')) ?>
				<div class="row">
					<div class="col-md-4">
						<h3 class="form-title">Data Profil</h3>						
					</div>
					<div class="col-md-8">	
						<div class="form-group">
							<label>Nama</label>
							<?= $this->form->text('profile_name', null, 'class="form-control"') ?>
						</div>	
						<div class="form-group">
							<label>E-mail</label>
							<?= $this->form->text('email', null, 'class="form-control"') ?>
						</div>	
						<div class="form-group">
							<button class="btn btn-success"><i class="fa fa-check"></i> Perbarui Profil</button>
						</div>
					</div>
				</div>
			<?= $this->form->close() ?>			
			<hr>
			<?= $this->form->open(routes()->name('setting_change_password')) ?>
				<div class="row">
					<div class="col-md-4">
						<h3 class="form-title">Ubah Password</h3>						
					</div>
					<div class="col-md-8">					
						<div class="form-group">
							<label>Password Lama</label>
							<?= $this->form->password('old_password', null, 'class="form-control"') ?>
						</div>
						<div class="form-group">
							<label>Password Baru</label>
							<?= $this->form->password('new_password', null, 'class="form-control"') ?>
						</div>
						<div class="form-group">
							<label>Ulangi Password Baru</label>
							<?= $this->form->password('confirm_new_password', null, 'class="form-control"') ?>
						</div>	
						<div class="form-group">
							<button class="btn btn-success"><i class="fa fa-key"></i> Ubah Password</button>
						</div>									
					</div>
				</div>
			<?= $this->form->close() ?>
		</div>		
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>
