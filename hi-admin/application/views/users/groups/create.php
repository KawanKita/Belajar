<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah Grup</h3>
        </div>                            
		<?= $this->form->open(routes()->name('usr_groups_store'), 'class="form-horizontal"') ?>
			<?php getview('users/groups/form') ?>
		<?= $this->form->close() ?>
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>