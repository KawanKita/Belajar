<?php section('content') ?>
    <?php getview('layouts/partials/message') ?>
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Data Grup</h3>
        </div>                    
        <div class="box-body box-spacing">
            <div class="toolbar">
                <div class="row">                                
                    <div class="col-md-2">
                        <a href="<?= routes()->name('usr_groups_create') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Grup Baru</a>
                    </div>                            
                </div>
            </div>
            <div class="table-responsive">
                <table id="data-table" class="table table-hover table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Grup</th>       
                            <th>Keterangan</th>                                                 
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($result as $row) { ?>
                            <tr>
                                <td>
                                    <?= $row->group ?><br>
                                    <a href="<?= routes()->name('usr_groups_access', array('id' => $row->id)) ?>"><i class="fa fa-key"></i> Akses</a>
                                    |
                                    <a href="<?= routes()->name('usr_groups_edit', array('id' => $row->id)) ?>"><i class="fa fa-pencil"></i> Edit</a>
                                    |
                                    <a href="javascript:void(0)" onclick="swalConfirm('Apakah anda yakin menghapus grup ini?', '<?= routes()->name('usr_groups_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                                <td><?= $row->description ?></td>                                       
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>  
<?php endsection() ?>

<?php section('page_script') ?>
<script>
    $(function() {
        $('#data-table').dataTable({
            columns 
        });
    });
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>