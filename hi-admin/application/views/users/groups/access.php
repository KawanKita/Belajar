<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Pengaturan Akses</h3>
        </div>   
		<?= $this->form->model($result, routes()->name('usr_groups_update_access', array('id' => $result->id)), 'class="form-horizontal"') ?>
			<div class="box-body box-spacing">
				<?= getview('layouts/partials/message') ?>
				<?= getview('layouts/partials/validation') ?>	
				<div class="row">
					<div class="col-md-12">					
						<div class="form-group">
							<label class="col-md-2 control-label">Grup</label>
							<div class="col-md-10">
								<?= $this->form->text('group', null, 'class="form-control" readonly') ?>
							</div>
						</div>										
					</div>
				</div>													
				<table id="data-table" class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th colspan="2" width="250px">Module/Menu</th>							
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rs_access as $access) { ?>
							<tr>
								<td colspan="2"><?= $access->access ?></td>
							</tr>
							<?php foreach ($access->rs_access_key as $access_key) { ?>
								<tr>
									<td width="1px">
										<?= $this->form->checkbox('access['.$access_key->access_key.']', '1') ?>
									</td>
									<td><?= $access_key->description ?></td>									
								</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>											
			</div>
			<div class="box-footer text-right">				
				<button class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
				<a href="<?= routes()->name('usr_groups') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>				
			</div>
		<?= $this->form->close() ?>
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>