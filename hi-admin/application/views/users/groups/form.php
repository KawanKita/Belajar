<div class="box-body box-spacing">
	<?= getview('layouts/partials/message') ?>
	<?= getview('layouts/partials/validation') ?>			
	<div class="form-group">
		<label class="col-md-2 control-label">Grup</label>
		<div class="col-md-10">
			<?= form()->text('group', null, 'class="form-control"') ?>
		</div>
	</div>	
	<div class="form-group">
		<label class="col-md-2 control-label">Keterangan</label>
		<div class="col-md-10">
			<?= form()->textarea('description', null, 'class="form-control"') ?>
		</div>
	</div>				
</div>
<div class="box-footer text-right">				
	<button class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
	<a href="<?= routes()->name('usr_groups') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>				
</div>