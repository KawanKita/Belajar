<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Grup</h3>
        </div>                            
		<?= $this->form->model($result, routes()->name('usr_groups_update', array('id' => $result->id)), 'class="form-horizontal"') ?>
			<?php getview('users/groups/form') ?>
		<?= $this->form->close() ?>
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>