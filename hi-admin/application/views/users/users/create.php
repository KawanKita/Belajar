<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Tambah User</h3>
        </div>                            
		<?= $this->form->open(routes()->name('usr_users_store'), 'class="form-horizontal"') ?>
			<div class="box-body box-spacing">
				<?= getview('layouts/partials/message') ?>
				<?= getview('layouts/partials/validation') ?>										
				<div class="form-group">
					<label class="col-md-2 control-label">Username</label>
					<div class="col-md-10">
						<?= $this->form->text('username', null, 'class="form-control"') ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">E-mail</label>
					<div class="col-md-10">
						<?= $this->form->text('email', null, 'class="form-control"') ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Grup</label>
					<div class="col-md-10">
						<?= $this->form->select('group_id', $ls_groups, null, 'class="form-control"') ?>
					</div>
				</div>	
				<div class="form-group">								
					<label class="col-md-2 control-label">Password</label>
					<div class="col-md-4">
						<?= $this->form->password('password', null, 'class="form-control"') ?>					
					</div>
					<label class="col-md-2 control-label">Ulangi Password</label>
					<div class="col-md-4">
						<?= $this->form->password('confirm_password', null, 'class="form-control"') ?>					
					</div>
				</div>							
				<div class="form-group">
					<label class="col-md-2 control-label">Nama</label>
					<div class="col-md-10">
						<?= $this->form->text('profile_name', null, 'class="form-control"') ?>
					</div>
				</div>												
			</div>				
			<div class="box-footer text-right">				
				<button class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
				<a href="<?= routes()->name('usr_users') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>				
			</div>	
		<?= $this->form->close() ?>
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>