<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Edit User</h3>
        </div>                    
        <div class="box-body box-spacing">
			<?= getview('layouts/partials/message') ?>
			<?= getview('layouts/partials/validation') ?>
			<?= $this->form->model($result, routes()->name('usr_users_update_profile', array('id' => $result->id))) ?>
				<div class="row">
					<div class="col-md-4">
						<h3 class="form-title">Data Profil</h3>						
					</div>
					<div class="col-md-8">	
						<div class="form-group">
							<label>Nama</label>
							<?= $this->form->text('profile_name', null, 'class="form-control"') ?>
						</div>	
						<div class="form-group">
							<button class="btn btn-success"><i class="fa fa-check"></i> Perbarui Profil</button>
						</div>
					</div>
				</div>
			<?= $this->form->close() ?>
			<hr>
			<?= $this->form->model($result, routes()->name('usr_users_update_setting', array('id' => $result->id))) ?>
				<div class="row">
					<div class="col-md-4">
						<h3 class="form-title">Pengaturan Akun</h3>						
					</div>
					<div class="col-md-8">	
						<div class="form-group">
							<label>Username</label>
							<?= $this->form->text('username', null, 'class="form-control" readonly') ?>
						</div>
						<div class="form-group">
							<label>E-mail</label>
							<?= $this->form->text('email', null, 'class="form-control"') ?>
						</div>
						<div class="form-group">
							<label>Grup</label>
							<?= $this->form->select('group_id', $ls_groups, null, 'class="form-control"') ?>
						</div>
						<div class="form-group">
							<button class="btn btn-success"><i class="fa fa-cog"></i> Simpan Perubahan</button>
						</div>
					</div>
				</div>
			<?= $this->form->close() ?>
			<hr>
			<?= $this->form->model($result, routes()->name('usr_users_change_password', array('id' => $result->id))) ?>
				<div class="row">
					<div class="col-md-4">
						<h3 class="form-title">Ubah Password</h3>						
					</div>
					<div class="col-md-8">											
						<div class="row">
							<div class="form-group col-md-6">
								<label>Password</label>
								<?= $this->form->password('password', null, 'class="form-control"') ?>
							</div>
							<div class="form-group col-md-6">
								<label>Ulangi Password</label>
								<?= $this->form->password('confirm_password', null, 'class="form-control"') ?>
							</div>
						</div>														
						<div class="form-group">
							<button class="btn btn-success"><i class="fa fa-key"></i> Ubah Password</button>
						</div>
					</div>
				</div>
			<?= $this->form->close() ?>
		</div>
		<div class="box-footer text-right">							
			<a href="<?= routes()->name('usr_users') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Batal</a>				
		</div>
	</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>