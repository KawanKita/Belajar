<?php section('content') ?>
	<div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Data User</h3>
        </div> 
		<div class="box-body box-spacing">
			<?= getview('layouts/partials/message') ?>
			<div class="toolbar">
				<div class="row">
					<div class="col-md-2">
						<a href="<?= routes()->name('usr_users_create') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah User Baru</a>
					</div>
					<div class="col-md-10 text-right">
						<form class="form-inline">
							<div class="form-group">
								<?= form()->select('group', $ls_groups, $this->input->get('group'), 'class="form-control input-sm"') ?>
							</div>
							<div class="form-group">
								<button class="btn btn-default btn-sm">Filter</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<table id="data-table" class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th width="200px">Username</th>
							<th>Nama Profil</th>
							<th width="120px">Grup</th>
							<th width="150px">Login Terakhir</th>						
						</tr>
					</thead>
					<tbody>
						<?php foreach ($result as $row) { ?>
							<tr>
								<td>
									<?= $row->username ?><br>									
                                    <a href="<?= routes()->name('usr_users_edit', array('id' => $row->id)) ?>"><i class="fa fa-pencil"></i> Edit</a>
                                    |
                                    <a href="javascript:void(0)" onclick="swalConfirm('Apakah anda yakin menghapus user ini?', '<?= routes()->name('usr_users_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>								
								</td>
								<td><?= $row->profile_name ?></td>
								<td><?= $row->group ?></td>
								<td><?= locale_human_date($row->last_login) ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php endsection() ?>

<?php section('page_script') ?>
<script>
$(function() {

	$('#data-table').dataTable();	

});
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>