<?php section('page_script') ?>
<script>
    $(function() {
        $('#data-table').dataTable({        	
        	order : [] 
        });
        $('#slug').slugify('#tag');
    });
</script>
<?php endsection() ?>