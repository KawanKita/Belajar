<?php section('content') ?>
<div class="row">
	<div class="col-md-3">		
		<?= form()->model($tag, routes()->name('cms_tags_update', array('id' => $tag->id))) ?>
			<?php getview('cms/tags/form', array(
				'form_title' => 'Edit Tag'
			)) ?>
		<?= form()->close() ?>
	</div>
	<div class="col-md-9">
		<?php getview('cms/tags/data') ?>
	</div>
</div>        
<?php endsection() ?>
<?php getview('cms/tags/page_script') ?>
<?php getview('layouts/layout') ?>