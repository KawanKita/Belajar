<?php section('content') ?>
<div class="row">
	<div class="col-md-3">
		<?= form()->open(routes()->name('cms_tags_store')) ?>
			<?php getview('cms/tags/form', array(
				'form_title' => 'Tambah Tag'
			)) ?>
		<?= form()->close() ?>
	</div>
	<div class="col-md-9">
		<?php getview('cms/tags/data') ?>
	</div>
</div>        
<?php endsection() ?>
<?php getview('cms/tags/page_script') ?>
<?php getview('layouts/layout') ?>