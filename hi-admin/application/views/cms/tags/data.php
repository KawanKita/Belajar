<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Data Tag</h3>
    </div>                    
    <div class="box-body box-spacing">
        <?= getview('layouts/partials/message') ?>
        <?= getview('layouts/partials/validation') ?>
        <table id="data-table" class="table table-hover table-bordered table-condensed">
        	<thead>
        		<th width="400px">Tag</th>        		
        		<th>Keterangan</th>
        	</thead>
        	<tbody>
        		<?php foreach ($result as $row) { ?>
        			<tr>
        				<td>        					
    						<?= $row->tag ?><br>        		                            			
                            <a href="<?= routes()->name('cms_tags_edit', array('id' => $row->id)) ?>"><i class="fa fa-edit"></i> Edit</a>
                            |
                            <a href="javascript:void(0)" onclick="swalConfirm('Kategori yang dihapus tidak bisa dikembalikan lagi?', '<?= routes()->name('cms_tags_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>        					
        				</td>        				
        				<td><?= $row->description ?></td>
        			</tr>
        		<?php } ?>
        	</tbody>
        </table>
    </div>                    
</div>    