<?php section('content') ?>
<div class="row">
	<div class="col-md-3">
		<?= form()->open(routes()->name('cms_categories_store')) ?>
			<?php getview('cms/categories/form', array(
				'form_title' => 'Tambah Kategori'
			)) ?>
		<?= form()->close() ?>
	</div>
	<div class="col-md-9">
		<?php getview('cms/categories/data') ?>
	</div>
</div>        
<?php endsection() ?>
<?php getview('cms/categories/page_script') ?>
<?php getview('layouts/layout') ?>