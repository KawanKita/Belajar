<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $form_title ?></h3>
    </div>                    
    <div class="box-body box-spacing">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Kategori</label>
                    <?= form()->text('category', null, 'id="category" class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Slug</label>                    
                    <?= form()->text('slug', null, 'id="slug" class="form-control"') ?>                    
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Sub Dari</label>                    
                    <?= form()->select('parent_id', $ls_categories, null, 'class="form-control"') ?>                    
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">        
                    <label>Kategori</label>                             
                    <?= form()->textarea('description', null, 'class="form-control" style="height:100px"') ?>
                </div>
            </div>
        </div>
    </div>                                      
    <div class="box-footer text-right">
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
        <a href="<?= routes()->name('cms_categories') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
    </div>  
</div>