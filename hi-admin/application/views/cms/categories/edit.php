<?php section('content') ?>
<div class="row">
	<div class="col-md-3">	
		<?= form()->model($category, routes()->name('cms_categories_update', array('id' => $category->id))) ?>
			<?php getview('cms/categories/form', array(
				'form_title' => 'Edit Kategori'
			)) ?>
		<?= form()->close() ?>
	</div>
	<div class="col-md-9">
		<?php getview('cms/categories/data') ?>
	</div>
</div>        
<?php endsection() ?>
<?php getview('cms/categories/page_script') ?>
<?php getview('layouts/layout') ?>