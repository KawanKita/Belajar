<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Data Kategori</h3>
    </div>                    
    <div class="box-body box-spacing">
        <?= getview('layouts/partials/message') ?>
        <?= getview('layouts/partials/validation') ?>
        <table id="data-table" class="table table-hover table-bordered table-condensed">
        	<thead>
        		<th width="400px">Kategori</th>        		
        		<th>Keterangan</th>
        	</thead>
        	<tbody>
        		<?php foreach ($result as $row) { ?>
        			<tr>
        				<td>
        					<div style="padding-left: <?= $row->tree_level * 20 ?>px">
        						<?= $row->category ?><br>        						
                                <a href="<?= routes()->name('cms_categories_edit', array('id' => $row->id)) ?>"><i class="fa fa-edit"></i> Edit</a>
                                |
                                <a href="javascript:void(0)" onclick="swalConfirm('Kategori yang dihapus tidak bisa dikembalikan lagi?', '<?= routes()->name('cms_categories_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>
        					</div>
        				</td>        				
        				<td><?= $row->description ?></td>
        			</tr>
        		<?php } ?>
        	</tbody>
        </table>
    </div>                    
</div>    