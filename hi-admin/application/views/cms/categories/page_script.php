<?php section('page_script') ?>
<script>
    $(function() {
        $('#data-table').dataTable({        	
        	ordering : false  
        });
        $('#slug').slugify('#category');
    });
</script>
<?php endsection() ?>