<div class="row">
    <div class="col-md-9">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $form_title ?></h3>
            </div>                    
            <div class="box-body box-spacing">
                <?= getview('layouts/partials/message') ?>
                <?= getview('layouts/partials/validation') ?>
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Judul</label>
                            <?= form()->text('title', null, 'id="title" class="form-control"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Slug</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <?= getConfig('base_url_site') ?>
                                </span>
                                <?= form()->text('slug', null, 'id="slug" class="form-control"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">                                     
                            <textarea name="content" id="content"><?= form()->data('content') ?></textarea>
                        </div>
                    </div>
                </div>
            </div>                                      
        </div>
    </div>
    <div class="col-sm-3">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Data Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="form-group">
                    <label>Tanggal Post</label>
                    <?= form()->text('post_date', date('Y-m-d'), 'class="form-control datepicker"') ?>
                </div>
                <div class="form-group">
                    <label>Kategori</label>
                    <?= form()->select('status', getConfig('cms_post_status'), null, 'class="form-control"') ?>
                </div>                        
            </div>    
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
                <a href="<?= routes()->name('cms_posts') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>                
        </div>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Jenis Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="btn-group-vertical btn-block" data-toggle="buttons">
                    <?php foreach(getConfig('cms_post_type') as $key => $type) { ?>
                        <label class="btn btn-primary <?= (form()->data('type') == $key) ? 'active' : '' ?>">
                            <?= form()->radio('type', $key, (form()->data('type') == $key) ? true : false) ?> <?= $type   ?>
                        </label>
                    <?php } ?>                            
                </div>
            </div>                    
        </div>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Format Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <input type="hidden" name="format" value="page">
                <div class="btn-group-vertical btn-block">                            
                    <a href="?format=standart" class="btn btn-primary"><i class="fa fa-thumb-tack"></i> Standart</a>
                    <a href="#" class="btn btn-primary active"><i class="fa fa-pencil"></i> Page</a>
                    <a href="?format=image" class="btn btn-primary"><i class="fa fa-photo"></i> Image</a>
                </div>
            </div>                    
        </div>                       
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Gambar Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="form-group text-center">
                    <input type="hidden" name="main_image" id="main_image" value="<?= form()->data('main_image') ?>">
                    <img src="<?= (form()->data('main_image')) ? form()->data('main_image') : base_url('public/img/no_image_available.jpg') ?>" id="main_image_preview" class="img-responsive">
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-default btn-sm" onclick="browse_main_image()"><i class="fa fa-folder-open"></i> Browse</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="clear_main_image()"><i class="fa fa-times"></i> Clear</button>
                </div>
            </div>                    
        </div>
    </div>
</div>    
<div id="filemanager" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">File Manager</h4>
            </div>
            <div class="modal-body"  style="padding: 0px;">
                <iframe name="filemanager" width="100%" height="500" style="border: 0px;" src="<?= base_url('filemanager/dialog.php?type=0&field_id=main_image') ?>"></iframe>
            </div>      
        </div>
    </div>
</div>