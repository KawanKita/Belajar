<?php section('page_script') ?>
<script>
    $(function () {
    	$('#slug').slugify('#title');
    	var toolbarContent = [
		    { name: 'document', items : [ 'Source'] },
		    { name: 'clipboard', items : ['PasteText','PasteFromWord','-','Undo','Redo' ] },		    	
		    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','RemoveFormat' ] },		    	   
		    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		    { name: 'links', items : [ 'Link','Unlink' ] },
		    { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak' ] },		    
		    { name: 'styles', items : [ 'Format', ] },
		    { name: 'colors', items : [ 'TextColor','BGColor' ] },
		    { name: 'tools', items : [ 'Maximize','-','About' ] }
		];
        CKEDITOR.replace('content', {
        	toolbar : toolbarContent,
	        filebrowserBrowseUrl : base_url('filemanager/dialog.php?type=2&editor=ckeditor&fldr='),
	        filebrowserUploadUrl : base_url('filemanager/dialog.php?type=2&editor=ckeditor&fldr='),
	        filebrowserImageBrowseUrl : base_url('filemanager/dialog.php?type=1&editor=ckeditor&fldr='),
            height : '500px'
        });
    });

    function browse_main_image() {
    	$('#filemanager').modal('show');
    }

    function clear_main_image() {
    	$('#main_image').val('');
    	$('#main_image_preview').prop('src', base_url('public/img/no_image_available.jpg'));
    }

    function responsive_filemanager_callback(field_id){		
		$('#main_image_preview').prop('src', $('#main_image').val());
	}
</script>
<?php endsection() ?>