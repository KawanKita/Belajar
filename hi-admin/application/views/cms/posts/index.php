<?php section('content') ?>    
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Data Post</h3>
    </div>                    
    <div class="box-body box-spacing">
        <?php getview('layouts/partials/message') ?>
        <div class="toolbar">
            <div class="row">                                
                <div class="col-md-2">
                    <a href="<?= routes()->name('cms_posts_create') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Data Pos Baru</a>
                </div>
                <div class="col-md-10 text-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <?= form()->select('archive', $ls_archive, $this->input->get('archive'), 'class="form-control input-sm"') ?>
                        </div>
                        <div class="form-group">
                            <?= form()->select('type', getLists('cms_post_type', 'Semua Jenis'), $this->input->get('type'), 'class="form-control input-sm"') ?>
                        </div>
                        <div class="form-group">
                            <?= form()->select('category', $ls_categories, $this->input->get('category'), 'class="form-control input-sm"') ?>
                        </div>
                        <div class="form-group">
                            <?= form()->select('tag', $ls_tags, $this->input->get('tag'), 'class="form-control input-sm"') ?>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default btn-sm">Filter</button>
                        </div>
                        <div class="form-group">
                            <div class="btn-group">
                                <a href="<?= routes()->name('cms_posts') ?>" class="btn btn-default btn-sm">Semua</a>
                                <?php foreach($rs_status as $status) { ?>
                                    <a href="<?= routes()->name('cms_posts', array('status' => $status->status)) ?>" class="btn btn-default btn-sm"><?= getConfig('cms_post_status')[$status->status] ?> (<?= $status->total ?>)</a>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="data-table" class="table table-hover table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Judul</th>                                        
                        <th width="200px" class="text-center">Kategori</th>
                        <th width="200px" class="text-center">Tag</th>
                        <th width="100px" class="text-center">Status</th>
                        <th width="150px" class="text-center">Tgl.Post</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php foreach ($result as $row) { ?>    
                        <tr>
                            <td>
                                <b><?= $row->title ?></b> by <label class="label label-danger"><?= $row->profile_name ?></label><br>
                                <small class="text-blue"><?= getConfig('base_url_site') . $row->slug ?></small><br>
                                <a href="<?= routes()->name('cms_posts_edit', array('id' => $row->id)) ?>"><i class="fa fa-edit"></i> Edit</a>
                                |
                                <a href="javascript:void(0)" onclick="swalConfirm('Postingan yang dihapus tidak bisa dikembalikan lagi?', '<?= routes()->name('cms_posts_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>
                                |
                                <?php if ($row->status == 'draft') { ?>
                                    <a href="javascript:void(0)" onclick="swalConfirm('Postingan yang sudah diteribitkan akan tampil pada website?', '<?= routes()->name('cms_posts_publish', array('id' => $row->id)) ?>')"><i class="fa fa-check"></i> Publish</a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="swalConfirm('Postingan yang sudah diubah menjadi draft akan hilang dari website?', '<?= routes()->name('cms_posts_draft', array('id' => $row->id)) ?>')"><i class="fa fa-list"></i> Draft</a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($row->categories as $category) { ?>
                                    <label class="label label-primary label-tag"><?= $category->category ?></label>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($row->tags as $tag) { ?>
                                    <div class="label label-primary label-tag"><?= $tag->tag ?></div>
                                <?php } ?>
                            </td>
                            <td class="text-center"><?= getConfig('cms_post_status')[$row->status] ?></td>
                            <td class="text-center"><?= locale_human_date($row->post_date) ?></td>
                        </tr>                               
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>                    
</div>        
<?php endsection() ?>

<?php section('page_script') ?>
<script>
    $(function() {
        $('#data-table').dataTable({
            order : []
        });
    });
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>