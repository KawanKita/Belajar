<?php section('content') ?>
<?= form()->open(routes()->name('cms_posts_store')) ?>
    <?php getview('cms/posts/image/form', array(
        'form_title' => 'Tambah Post'
    )) ?>
<?= form()->close() ?>
<?php endsection() ?>
<?php getview('cms/posts/image/form_page_script') ?>
<?php getview('layouts/layout') ?>