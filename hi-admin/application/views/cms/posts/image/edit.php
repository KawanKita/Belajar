<?php section('content') ?>
<?= form()->model($result, routes()->name('cms_posts_update', array('id' => $result->id))) ?>
    <?php getview('cms/posts/image/form', array(
    	'form_title' => 'Edit Post'
   	)) ?>
<?= form()->close() ?>
<?php endsection() ?>
<?php getview('cms/posts/image/form_page_script') ?>
<?php getview('layouts/layout') ?>