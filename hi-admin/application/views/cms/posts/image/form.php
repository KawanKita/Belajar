<div class="row">
    <div class="col-md-9">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $form_title ?></h3>
            </div>                    
            <div class="box-body box-spacing">
                <?= getview('layouts/partials/message') ?>
                <?= getview('layouts/partials/validation') ?>
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Judul</label>
                            <?= form()->text('title', null, 'id="title" class="form-control"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Slug</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <?= getConfig('base_url_site') ?>
                                </span>
                                <?= form()->text('slug', null, 'id="slug" class="form-control"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">                                     
                            <textarea name="content" id="content"><?= form()->data('content') ?></textarea>
                        </div>
                    </div>
                </div>
            </div>                                                          
        </div>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Data Image</h3>
            </div>                    
            <div class="box-body box-spacing">                        
                <div id="post-image" class="row post-image">
                    <?php if (count(form()->data('images')) > 0) { ?>
                        <?php $count_post_images = 0 ?>
                        <?php foreach(form()->data('images') as $image) { ?>   
                            <?php $count_post_images++; ?>                             
                            <div id="post-image-list-<?= $count_post_images ?>" class="col-md-4 post-image-list">
                                <div class="post-image-image">
                                    <img src="<?= $image['image_url'] ?>" />   
                                    <input type="hidden" name="images[<?= $count_post_images ?>][image_url]" value="<?= $image['image_url'] ?>">
                                    <input type="hidden" name="images[<?= $count_post_images ?>][caption]" value="<?= $image['caption'] ?>" id="post-image-list-caption-<?= $count_post_images ?>">
                                    <input type="hidden" name="images[<?= $count_post_images ?>][link]" value="<?= $image['link'] ?>" id="post-image-list-link-<?= $count_post_images ?>">
                                    <input type="hidden" name="images[<?= $count_post_images ?>][description]" value="<?= $image['description'] ?>" id="post-image-list-description-<?= $count_post_images ?>">
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm" onclick="edit_post_image(<?= $count_post_images ?>)"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-default btn-sm" onclick="delete_post_image(<?= $count_post_images ?>)"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>                                    
                        <?php } ?>
                    <?php } ?>
                    <div class="col-md-4">
                        <div class="post-image-add">
                            <a href="javascript:void(0)" onclick="browse_post_image()"><i class="fa fa-plus-circle"></i></a>
                            <input type="hidden" id="image-url-add">
                        </div>
                    </div>
                </div>
            </div>                                      
        </div>
    </div>
    <div class="col-sm-3">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Data Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="form-group">
                    <label>Tanggal Post</label>
                    <?= form()->text('post_date', date('Y-m-d'), 'class="form-control datepicker"') ?>
                </div>
                <div class="form-group">
                    <label>Kategori</label>
                    <?= form()->select('status', getConfig('cms_post_status'), null, 'class="form-control"') ?>
                </div>                        
            </div>    
            <div class="box-footer text-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
                <a href="<?= routes()->name('cms_posts') ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>                
        </div>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Jenis Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="btn-group-vertical btn-block" data-toggle="buttons">
                    <?php foreach(getConfig('cms_post_type') as $key => $type) { ?>
                        <label class="btn btn-primary <?= (form()->data('type') == $key) ? 'active' : '' ?>">
                            <?= form()->radio('type', $key, (form()->data('type') == $key) ? true : false) ?> <?= $type   ?>
                        </label>
                    <?php } ?>                            
                </div>
            </div>                    
        </div>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Format Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <input type="hidden" name="format" value="image">
                <div class="btn-group-vertical btn-block">                            
                    <a href="?format=post" class="btn btn-primary"><i class="fa fa-thumb-tack"></i> Standart</a>
                    <a href="?format=page" class="btn btn-primary"><i class="fa fa-pencil"></i> Page</a>
                    <a href="#" class="btn btn-primary active"><i class="fa fa-photo"></i> Image</a>
                </div>
            </div>                    
        </div>     
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Kategori</h3>
            </div>                    
            <div class="box-body box-spacing" style="height:200px; overflow-y: auto">
                <?php foreach ($rs_categories as $category) { ?>
                    <?= str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $category->tree_level) ?> <?= form()->checkbox('categories['.$category->id.']', $category->id) ?> <?= $category->category ?><br>
                <?php } ?>
            </div>                    
        </div> 
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tag</h3>
            </div>                    
            <div class="box-body box-spacing">
               <?= form()->multiselect('tags[]', $ls_tags, null, 'class="form-control select2"') ?>
            </div>                    
        </div>             
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Gambar Post</h3>
            </div>                    
            <div class="box-body box-spacing">
                <div class="form-group text-center">
                    <input type="hidden" name="main_image" id="main_image" value="<?= form()->data('main_image') ?>">
                    <img src="<?= (form()->data('main_image')) ? form()->data('main_image') : base_url('public/img/no_image_available.jpg') ?>" id="main_image_preview" class="img-responsive">
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn btn-default btn-sm" onclick="browse_main_image()"><i class="fa fa-folder-open"></i> Browse</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="clear_main_image()"><i class="fa fa-times"></i> Clear</button>
                </div>
            </div>                    
        </div>
    </div>
</div>    
<div id="filemanager" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">File Manager</h4>
            </div>
            <div class="modal-body"  style="padding: 0px;">
                <iframe name="filemanager" width="100%" height="500" style="border: 0px;" src="<?= base_url('filemanager/dialog.php?type=0&field_id=main_image') ?>"></iframe>
            </div>      
        </div>
    </div>
</div>
<div id="form-post-image" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Data Image</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="post-image-id">
                <div class="form-group">
                    <label>Caption</label>
                    <input type="text" id="post-image-caption" class="form-control">
                </div>                
                <div class="form-group">
                    <label>Link</label>
                    <input type="text" id="post-image-link" class="form-control">
                </div>                
                <div class="form-group">   
                    <label>Keterangan</label>                 
                    <textarea id="post-image-description" class="form-control"></textarea>
                </div>                
            </div>      
            <div class="modal-footer">
                <button type="button" id="post-image-save" class="btn btn-success" onclick="save_post_image()"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" id="post-image-save" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply"></i> Batal</button>
            </div>
        </div>
    </div>
</div>