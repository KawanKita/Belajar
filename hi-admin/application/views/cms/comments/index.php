<?php section('content') ?>    
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Data Komentar</h3>
    </div>                    
    <div class="box-body box-spacing">
        <?= getview('layouts/partials/message') ?>
        <?= getview('layouts/partials/validation') ?>
        <div class="toolbar">
            <div class="row">                                
                <div class="col-md-12 text-right">
                    <form class="form-inline">                        
                        <div class="form-group">
                            <?= form()->select('type', getLists('cms_comment_type', 'Semua Jenis'), $this->input->get('type'), 'class="form-control input-sm"') ?>
                        </div>                           
                        <div class="form-group">
                            <button class="btn btn-default btn-sm">Filter</button>
                        </div>
                        <div class="form-group">
                            <div class="btn-group">
                                <a href="<?= routes()->name('cms_comments') ?>" class="btn btn-default btn-sm">Semua</a>
                                <?php foreach ($rs_status as $status) { ?>
                                    <a href="<?= routes()->name('cms_comments', array('status' => $status->status)) ?>" class="btn btn-default btn-sm"><?= getConfig('cms_comment_status')[$status->status] ?> (<?= $status->total ?>)</a>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="data-table" class="table table-hover table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="200px">Penulis</th>                                        
                        <th >Komentar</th>             
                        <th width="100px" class="text-center">Type</th>           
                        <th width="100px" class="text-center">Status</th>
                        <th width="150px" class="text-center">Tgl.Komentar</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php foreach ($result as $row) { ?>    
                        <tr>
                            <td>
                                <b><?= $row->name ?></b> <?= ($row->is_admin == 'true') ? '<label class="label label-danger">Admin</label>' : '' ?><br>                                
                                <small class="text-blue">E-mail :  <?= $row->email ?></small>
                            </td>
                            <td>
                                <?= $row->comment ?><br>                               
                                <?= form()->open(routes()->name('cms_comments_reply', array('id' => $row->id)), 'id="reply-'.$row->id.'" style="display:none"') ?>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="padding: 5px 0px"><?= form()->textarea('comment', null, 'class="form-control" style="width:100%;height:100px;"') ?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px 0px ">
                                                <button class="btn btn-success btn-sm"><i class="fa fa-comments"></i> Balas</button>
                                                <button type="button" class="btn btn-default btn-sm" onclick="cancel_reply(<?= $row->id ?>)"><i class="fa fa-reply"></i> Batal</button>
                                            </td>
                                        </tr>
                                    </table>
                                <?= form()->close() ?>
                                <a href="javascript:void(0)" onclick="reply(<?= $row->id ?>)"><i class="fa fa-reply"></i> Reply</a>
                                |
                                <a href="javascript:void(0)" onclick="swalConfirm('Komentar yang dihapus tidak bisa dikembalikan lagi?', '<?= routes()->name('cms_comments_delete', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Delete</a>
                                |
                                <?php if($row->status == 'wappr') { ?>
                                    <a href="javascript:void(0)" onclick="swalConfirm('Komentar yang disetujui akan tampak pada website?', '<?= routes()->name('cms_comments_appr', array('id' => $row->id)) ?>')"><i class="fa fa-check"></i> Setujui</a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="swalConfirm('Komentar yang dibatalkan persetujuanya akan hilang dari website?', '<?= routes()->name('cms_comments_wappr', array('id' => $row->id)) ?>')"><i class="fa fa-times"></i> Batalkan Persetujuan</a>
                                <?php } ?>
                            </td>
                            <td class="text-center">
                                <?= getConfig('cms_comment_type')[$row->type] ?>
                            </td>
                            <td class="text-center"><?= getConfig('cms_comment_status')[$row->status] ?></td>
                            <td class="text-center"><?= locale_human_date($row->comment_date) ?></td>
                        </tr>                               
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>                    
</div>        
<?php endsection() ?>

<?php section('page_script') ?>
<script>
    $(function() {
        $('#data-table').dataTable({
            order : []
        });
    });
    function reply(id) {
        $('#reply-' + id).show();
    }
    function cancel_reply(id) {
        $('#reply-' +id).hide();
    }
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>