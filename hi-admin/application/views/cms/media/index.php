<?php section('content') ?>    
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Media</h3>
    </div>                    
    <div class="box-body box-spacing" style="padding: 0px;">
        <iframe src="<?= base_url('filemanager/dialog.php') ?>" width="100%" height="550px" style="border:0px"></iframe>
    </div>                    
</div>        
<?php endsection() ?>

<?php getview('layouts/layout') ?>