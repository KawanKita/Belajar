<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <title><?= getConfig('_application_name') ?></title>
		<link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/plugins/font-awesome-4.5.0/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/plugins/sweetalert/sweetalert.css') ?> ">
		<link rel="stylesheet" href="<?= base_url('public/plugins/jquery.growl/jquery.growl.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/plugins/datepicker/datepicker3.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/plugins/timepicker/bootstrap-timepicker.min.css') ?> ">
		<link rel="stylesheet" href="<?= base_url('public/plugins/select2/select2.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/plugins/datatables/dataTables.bootstrap.css') ?>">
		<?php render('css') ?>	
		<link rel="stylesheet" href="<?= base_url('public/css/AdminLTE.min.css') ?>">
		<link rel="stylesheet" href="<?= base_url('public/css/skins/_all-skins.min.css') ?>">  	
		<link rel="stylesheet" href="<?= base_url('public/css/style.css') ?>">
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

  	<body class="hold-transition skin-red sidebar-mini">
    	<div class="wrapper">
      		<header class="main-header">
        		<a href="<?= routes()->name('dashboard') ?>" class="logo">
		        	<span class="logo-mini"><?= getConfig('_application_short_name') ?></span>
		          	<span class="logo-lg"><?= getConfig('_application_name') ?></span>
		        </a>
		        <nav class="navbar navbar-static-top" role="navigation">
			        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			        </a>
			        <div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= getLogin('profile_name') ?></a>
								<ul class="dropdown-menu">																		
									<li><a href="<?= routes()->name('setting') ?>"><i class="fa fa-cog"></i> Pengaturan</a></li>
									<li><a href="<?= routes()->name('logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
		        </nav>
		    </header>
      		<aside class="main-sidebar">
        		<section class="sidebar">
        			<ul class="sidebar-menu">
        				<li>
        					<a href="<?= routes()->name('dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
				        </li>      
				        <li>
        					<a href="<?= routes()->name('cms_tags') ?>"><i class="fa fa-hashtag"></i> <span>Tag</span></a>
				        </li>      
				        <li>
        					<a href="<?= routes()->name('cms_categories') ?>"><i class="fa fa-tag"></i> <span>Kategori</span></a>
				        </li>      
        				<li class="treeview">
		              		<a href="#">
		                		<i class="fa fa-pencil"></i> <span>Post</span> <i class="fa fa-angle-left pull-right"></i>
		              		</a>
		              		<ul class="treeview-menu">
				                <li><a href="<?= routes()->name('cms_posts') ?>"><i class="fa fa-circle-o"></i>Data Post</a></li>
				                <li><a href="<?= routes()->name('cms_posts_create') ?>"><i class="fa fa-circle-o"></i>Tambah Post</a></li>
				            </ul>
						</li>	
						<li>
        					<a href="<?= routes()->name('cms_media') ?>"><i class="fa fa-photo"></i> <span>Media</span></a>
				        </li> 
						<li>
        					<a href="<?= routes()->name('cms_comments') ?>"><i class="fa fa-comments"></i> <span>Komentar</span></a>
				        </li> 	
				        <li class="treeview">
		              		<a href="#">
		                		<i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
		              		</a>
		              		<ul class="treeview-menu">
				                <li><a href="<?= routes()->name('usr_groups') ?>"><i class="fa fa-circle-o"></i>Grup</a></li>
				                <li><a href="<?= routes()->name('usr_users') ?>"><i class="fa fa-circle-o"></i>User</a></li>
				            </ul>
						</li>				
		          	</ul>
		        </section>
      		</aside>
      		<div class="content-wrapper">
    			<section class="content">
					<?php render('content') ?>
				</section>
			</div>

			<footer class="main-footer">
        		<strong>Copyright &copy; 2016 <a href="javascript:;">Harmoni-Integra</a>.</strong>
      		</footer>
      		<aside class="control-sidebar control-sidebar-dark">
        		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          			<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        		</ul>
      		</aside>
      		<div class="control-sidebar-bg"></div>
    	</div>

    	<script src="<?= base_url('public/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
		<script src="<?= base_url('public/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('public/js/moment.min.js') ?>"></script>			
		<script src="<?= base_url('public/plugins/sweetalert/sweetalert.min.js') ?>"></script>
		<script src="<?= base_url('public/plugins/sweetalert/sweetalert.custom.js') ?>"></script>
		<script src="<?= base_url('public/plugins/jquery.growl/jquery.growl.js') ?>"></script>
		<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>	
		<script src="<?= base_url('public/plugins/select2/select2.min.js') ?>"></script>	
		<script src="<?= base_url('public/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
	    <script src="<?= base_url('public/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>	
	    <script src="<?= base_url('public/plugins/ckeditor/ckeditor.js') ?>"></script>
	    <script src="<?= base_url('public/plugins/slugify/speakingurl.js') ?>"></script>
	    <script src="<?= base_url('public/plugins/slugify/slugify.min.js') ?>"></script>
		<?php render('script') ?>
		<script src="<?= base_url('public/js/app.min.js') ?>"></script>
		<script src="<?= base_url('public/js/app/app.js') ?>"></script>
		<?php render('page_script') ?>
  	</body>
</html>