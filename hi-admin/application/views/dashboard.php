<?php section('content') ?>
<h3>Dashboard</h3>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="page-header">
			<h3>Selamat datang di <?= $this->config->item('_application_name') ?></h3>
		</div>

		<div class="row">
			<div class="col-md-4">
				<h4 class="text-bold">Kategori</h4>
				<ul class="list-unstyled">
					<li><a href="<?= routes()->name('cms_categories') ?>"><i class="fa fa-tag"></i> Tambah kategori</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h4 class="text-bold">Tag</h4>
				<ul class="list-unstyled">
					<li><a href="<?= routes()->name('cms_tags') ?>"><i class="fa fa-hashtag"></i> Tambah tag baru</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h4 class="text-bold">Post</h4>
				<ul class="list-unstyled">
					<li><a href="<?= routes()->name('cms_posts_create').'?format=image' ?>"><i class="fa fa-image"></i> Tambah galeri</a></li>
					<li><a href="<?= routes()->name('cms_posts_create').'?format=page' ?>"><i class="fa fa-file"></i> Tambah page</a></li>
					<li><a href="<?= routes()->name('cms_posts_create').'?format=standart' ?>"><i class="fa fa-pencil"></i> Tambah post</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body">
				<h4 class="text-bold">Posting belum terpublish</h4>
				<table class="table table-responsive table-bordered">
					<thead>
	                    <tr>
	                        <th>Judul</th>
	                        <th width="150px" class="text-center">Tgl.Post</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<?php foreach ($drafts as $row) { ?>    
	                        <tr>
	                            <td>
	                                <b><?= $row->title ?></b> by <label class="label label-danger"><?= $row->profile_name ?></label><br>
	                                <small class="text-blue"><?= getConfig('base_url_site') . $row->slug ?></small><br>
                                    <a href="javascript:void(0)" onclick="swalConfirm('Postingan yang sudah diteribitkan akan tampil pada website?', '<?= routes()->name('cms_posts_publish', array('id' => $row->id)) ?>')"><i class="fa fa-trash"></i> Publish</a>
	                            </td>
	                            <td><?= locale_human_date($row->post_date) ?></td>
	                        </tr>                               
	                    <?php } ?>
	                </tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body">
				<h4 class="text-bold">Komentar belum terpublish</h4>
				<table class="table table-responsive table-bordered">
					<thead>
	                    <tr>
	                        <th>Komentar</th>                
	                        <th width="100px" class="text-center">Type</th>
	                        <th width="150px" class="text-center">Tgl.Komentar</th>
	                    </tr>
	                </thead>
	                <tbody>
	                	<?php foreach ($comments as $row) { ?>    
	                        <tr>
	                            <td>
	                                <b><?= $row->name ?></b> <?= ($row->is_admin == 'true') ? '<label class="label label-danger">Admin</label>' : '' ?><br>                                
	                                <small class="text-blue">E-mail :  <?= $row->email ?></small><br>
	                                <?= $row->comment ?><br>                               
	                                <a href="javascript:void(0)" onclick="swalConfirm('Komentar yang disetujui akan tampak pada website?', '<?= routes()->name('cms_comments_appr', array('id' => $row->id)) ?>')"><i class="fa fa-check"></i> Setujui</a>
	                            </td>
	                            <td class="text-center">
	                                <?= getConfig('cms_comment_type')[$row->type] ?>
	                            </td>
	                            <td class="text-center"><?= locale_human_date($row->comment_date) ?></td>
	                        </tr>                               
	                    <?php } ?>
	                </tbody>
				</table>
			</div>
		</div>
	</div>
</div>	
<?php endsection('content') ?>

<?php getview('layouts/layout') ?>