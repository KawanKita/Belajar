<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exceptions {

	protected $exceptions;

	protected $error;

	protected $message;

	public function __construct() {		
		$this->exceptions = &get_instance();
	}

	public function error($error, $message = null) {
		$this->error = $error;
		if ($message) {
			$this->message = $message;
		} else {
			if (lang($error)) {
				$this->message = lang($error);
			}
		}
		if (method_exists($this, $error)) {
            $this->$error();
        }  
	}

	public function get_error() {
		return $this->error;
	}

	public function get_message() {
		return $this->message;
	}

}
