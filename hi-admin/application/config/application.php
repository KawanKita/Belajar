<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['base_url_site'] = 'http://p2bj.prov-jatim.go.id/';
$config['cms_post_status'] = array(
	'draft' => 'Draft',
	'publish' => 'Publish'
);

$config['cms_post_type'] = array(
	'news' => 'Berita',
	'info' => 'Pengumuman',
	'page' => 'Page',
	'gallery' => 'Galeri'
);

$config['cms_post_format'] = array(
	'standart' => 'Standart',
	'page' => 'Page',
	'image' => 'Image'
);

$config['cms_comment_type'] = array(
	'general' => 'Umum',
	'pengaduan' => 'Pengaduan'
);

$config['cms_comment_status'] = array(
	'appr' => 'Disetujui',
	'wappr' => 'Menunggu'
);