<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
	'_application_name' => 'HI-CMS',
	'_application_short_name' => 'HI-CMS',

	'_date' => 'd-m-Y',
	'_time' => 'H:i:s',
	'_datetime' => 'd-m-Y H:i:s',
	'_human_date' => '{d} {m} {Y}',
	'_human_datetime' => '{d} {m} {Y} {H}:{i}',

	'_currency' => 'Rp ',

	'_thousand_separator' => '.',
	'_decimal_separator' => '.',

	'_day' => array(
		'1' => 'Senin',
		'2' => 'Selasa',
		'3' => 'Rabu',
		'4' => 'Kamis',
		'5' => 'Jum\'at',
		'6' => 'Sabtu',
		'7' => 'Minggu'
	),

	'_month' => array(
		'01' => 'Januari',
		'02' => 'Febuari',
		'03' => 'Maret',
		'04' => 'April',
		'05' => 'Mei',
		'06' => 'Juni',
		'07' => 'Juli',
		'08' => 'Agustus',
		'09' => 'September',
		'10' => 'Oktober',
		'11' => 'November',
		'12' => 'Desember'
	)

);