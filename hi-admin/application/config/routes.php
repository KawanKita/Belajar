<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'users/login';
$route['login/attemp'] = 'users/login/attemp';
$route['setting'] = 'users/login/setting';
$route['setting/change_profile'] = 'users/login/change_profile';
$route['setting/change_password'] = 'users/login/change_password';
$route['logout'] = 'users/login/logout';
$route['cms/posts/create'] = 'cms/posts/create';
$route['cms/posts/store'] = 'cms/posts/store';
$route['cms/posts/(:any)'] = 'cms/posts/index/$1';

$route['cms/comments/(:any)'] = 'cms/comments/index/$1';

$config['routes_name'] = array(
	'login' => 'login',
	'login_attemp' => 'login/attemp',
	'setting' => 'setting',
	'setting_change_profile' => 'setting/change_profile',
	'setting_change_password' => 'setting/change_password',
	'logout' => 'logout',
	'dashboard' => 'dashboard',
	'usr_groups' => 'users/groups',
	'usr_groups_create' => 'users/groups/create',
	'usr_groups_store' => 'users/groups/store',
	'usr_groups_edit' => 'users/groups/edit/(:id)',
	'usr_groups_update' => 'users/groups/update/(:id)',
	'usr_groups_delete' => 'users/groups/delete/(:id)',
	'usr_groups_access' => 'users/groups/access/(:id)',
	'usr_groups_update_access' => 'users/groups/update_access/(:id)',
	'usr_users' => 'users/users',
	'usr_users_create' => 'users/users/create',
	'usr_users_store' => 'users/users/store',	
	'usr_users' => 'users/users',
	'usr_users_edit' => 'users/users/edit/(:id)',
	'usr_users_update_profile' => 'users/users/update_profile/(:id)',	
	'usr_users_update_setting' => 'users/users/update_setting/(:id)',	
	'usr_users_change_password' => 'users/users/change_password/(:id)',	
	'usr_users_delete' => 'users/users/delete/(:id)',
	'cms' => 'cms',
	'cms_tags' => 'cms/tags',
	'cms_tags_create' => 'cms/tags/create',
	'cms_tags_store' => 'cms/tags/store',
	'cms_tags_edit' => 'cms/tags/edit/(:id)',
	'cms_tags_update' => 'cms/tags/update/(:id)',
	'cms_tags_delete' => 'cms/tags/delete/(:id)',
	'cms_categories' => 'cms/categories',
	'cms_categories_store' => 'cms/categories/store',
	'cms_categories_edit' => 'cms/categories/edit/(:id)',
	'cms_categories_update' => 'cms/categories/update/(:id)',
	'cms_categories_delete' => 'cms/categories/delete/(:id)',
	'cms_posts' => 'cms/posts/(:?status)',
	'cms_posts_create' => 'cms/posts/create',
	'cms_posts_store' => 'cms/posts/store',
	'cms_posts_edit' => 'cms/posts/edit/(:id)',
	'cms_posts_update' => 'cms/posts/update/(:id)',
	'cms_posts_delete' => 'cms/posts/delete/(:id)',
	'cms_posts_publish' => 'cms/posts/publish/(:id)',
	'cms_posts_draft' => 'cms/posts/draft/(:id)',
	'cms_media' => 'cms/media',
	'cms_comments' => 'cms/comments/(:?status)',
	'cms_comments_reply' => 'cms/comments/reply/(:id)',
	'cms_comments_delete' => 'cms/comments/delete/(:id)',
	'cms_comments_appr' => 'cms/comments/appr/(:id)',
	'cms_comments_wappr' => 'cms/comments/wappr/(:id)'
);
