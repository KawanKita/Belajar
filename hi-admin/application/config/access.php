<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['access'] = array(
	'dashboard/index' => array(
		'has_login' => true
	)
);