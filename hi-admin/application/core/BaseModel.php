<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseModel extends CI_Model {

    protected $table;

    protected $primary_key = 'id';    

    protected $fillable = array();

    protected $row_num = 'row_num';

    protected $auto_row_num = false;    

    protected $format_row_num = '{Y}{m}{d}:3';

    protected $tree = false;

    protected $parent_id = 'parent_id';

    protected $tree_id = 'tree_id';

    protected $tree_id_digit = '3';

    protected $set_default = array();

    protected $timestamp = false;

    protected $author = false;

    public function __construct() {
        parent::__construct();
    }

    public function get() {
        return $this->db->get($this->table)->result();
    }

    public function get_by($column, $key) {
        return $this->db->where($this->table . '.' .$column, $key)->get($this->table)->result();        
    }

    public function find($id) {
        return $this->db->where($this->table . '.' . $this->primary_key, $id)->get($this->table)->row();
    }

    public function find_by($column, $key) {
        return $this->db->where($this->table . '.' .$column, $key)->get($this->table)->row();
    }

    public function find_or_fail($id) {
        $result = $this->db->where($this->table . '.' .$this->primary_key, $id)->get($this->table)->row();
        if ($result) {
            return $result;
        } else {
            $this->model_exceptions->error('find_or_fail');        
        }
    }

    public function find_by_or_fail($column, $key) {
        $result = $this->db->where($this->table . '.' . $column, $key)->get($this->table)->row();
        if ($result) {
            return $result;
        } else {
            $this->model_exceptions->error('find_or_fail');
        }
    }

    public function insert($record) {
        foreach ($this->set_default as $key => $value) {
            if (!isset($record[$key])) {
                $record[$key] = $value;
            }
        }
        $record = $this->fillable($record);
        if ($this->auto_row_num) {
            $record[$this->row_num] = $this->generate_row_num();
        }
        if ($this->tree) {
            $record['parent_id'] = ($record['parent_id']) ? $record['parent_id'] : null;
            $record[$this->tree_id] = $this->generate_tree_id($record['parent_id']);
        }
        if ($this->author) {
            $record['created_by'] = getLogin('username');
        }
        if ($this->timestamp) {
            $record['created_at'] = date('Y-m-d');
        }        
        return $this->db->insert($this->table, $record);            
    }
    
    public function insert_id() {
        return $this->db->insert_id();
    }    
    
    public function insert_batch($record) {
        foreach ($record as $key => $row) {
            $record[$key] = $this->fillable($record);
            if ($this->author) {
            $record[$key]['created_by'] = getLogin('username');
            }
            if ($this->timestamp) {
                $record[$key]['created_at'] = date('Y-m-d');
            }  
        }
        return $this->db->insert_batch($this->table);
    }

    public function update($id, $record) {
        $record =  $this->fillable($record);
        if ($this->timestamp) {            
            $record['updated_at'] = date('Y-m-d H:i:s');
        }
        if ($this->author) {
            $record['updated_by'] = getLogin('username');
        }
        return $this->db->where($this->table . '.' . $this->primary_key, $id)->update($this->table, $record);        
    }

    public function delete($id) {
        return $this->db->where($this->table . '.' . $this->primary_key, $id)->delete($this->table);
    }    

    public function fillable($record) {
        $data = array();    
        foreach ($this->fillable as $fillable) { 
            $formatters = array();         
            $parse = explode(':', $fillable);
            if (count($parse) > 1) {
                $fillable = $parse[0];              
                $formatters = explode('|', $parse[1]);
            }          
            if (isset($record[$fillable])) {                   
                if ($record[$fillable]) {                      
                    if (count($formatters) <> 0) {
                        foreach ($formatters as $formatter) {
                            switch ($formatter) {
                                case 'date':
                                    $data[$fillable] = date('Y-m-d', strtotime($record[$fillable]));
                                break;
                                case 'upper':
                                    $data[$fillable] = strtoupper($data[$fillable]);
                                break;
                                case 'lower':
                                    $data[$fillable] = strtolower($data[$fillable]); 
                                case 'sha1':
                                    $data[$fillable] = sha1($data[$fillable]);
                                    break;                       
                                default:
                                    $data[$fillable] = $record[$fillable];
                                break;
                            }                
                        }
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                } else {
                    if ($record[$fillable] === '') {
                        $data[$fillable] = null;
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                }
            }
        }                    
        return $data;
    }

    public function generate_row_num() {
        $format = $this->format_row_num;
        $parse = explode(':', $format);
        $prefix = str_replace(array('{Y}', '{m}', '{d}'), array(date('Y'), date('m'), date('d')), $parse[0]);
        $digit = str_repeat('0', $parse[1]);
        $last_id =  $this->db->select_max($this->row_num)
        ->where('left('.$this->row_num.','.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->{$this->row_num};
        if ($last_id) {            
            $counter = substr($last_id, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }        
    }

    public function generate_tree_id($parent_id = null) {        
        $digit = str_repeat('0', $this->tree_id_digit);
        $last_id = $this->db->select_max($this->tree_id)
        ->where($this->parent_id, $parent_id)
        ->get($this->table)
        ->row()
        ->{$this->tree_id};
        if ($parent_id) {
            $prefix = $this->find($parent_id)->tree_id;
            if ($last_id) {
                $counter = $last_id+1;
                return $prefix.substr($digit.$counter, -strlen($digit));
            } else {
                return $prefix.substr($digit.'1', -strlen($digit));
            }
        } else {            
            if ($last_id) {
                $counter = $last_id+1;
                return substr($digit.$counter, -strlen($digit));
            } else {
                return substr($digit.'1', -strlen($digit));
            }
        }
    }

}