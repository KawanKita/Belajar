<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes {

	protected $CI;

	protected $routes;

	public function __construct($config = array()) {
		$this->CI = &get_instance();
		$this->routes = $config['routes_name'];				
	}

	public function name($name, $params = null, $query = false) {				
		if ($route_url = isset($this->routes[$name])) {
			$route_url = $this->build_route($this->routes[$name], $params, $query);			
			return base_url($route_url);
		} else {
			return base_url();
		}
	}

	public function to($route_url, $params, $query) {
	$route_url = $this->build_route($route_url, $params, $query);		
		return base_url($route_url);
	} 

	public function build_route($route_url, $params = null, $query = false) {		
		if ($params) {
			foreach ($params as $key => $value) {
				$route_url = str_replace(array('(:'.$key.')', '(:?'.$key.')'), $value, $route_url);
			}
		}
		$route_url = preg_replace('/\(:\?[a-z]*\)/', '', $route_url);	
		if ($query === TRUE) {
			$query = $this->CI->input->get();
		}
		if ($query) {
			foreach ($query as $key => $value) {
				if (strpos($route_url, '?')) {
					$route_url .= '&' . $key . '=' . $value;
				} else {
					$route_url .= '?' . $key . '=' . $value;
				}
			}			
		}
		return $route_url;		
	}

}

function routes() {
	$CI = &get_instance();
	return $CI->routes;
}