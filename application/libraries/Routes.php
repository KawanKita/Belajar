<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes {

	protected $CI;

	protected $routes;

	public function __construct($config = array()) {
		$this->CI = &get_instance();
		$this->routes = $config;
	}

	public function name($name, $params = null, $query = false) {		
		if ($route_url = isset($routes[$name])) {
			$route_url = $this->build_route($route_url, $params, $query);
			return $route_url;
		} else {
			return null;
		}
	}

	public function to($route_url, $params, $query) {
		return $route_url = $this->build_route($route_url, $params, $query);		
	} 

	public function build_route($route_url, $params = null, $query = false) {
		if ($params) {
			foreach ($params as $key => $value) {
				$route_url = str_replace('(:'.$key.')', $value);
			}
		}
		if ($query === TRUE) {
			$query = $this->CI->input->get();
		}
		if ($query) {
			foreach ($query as $key => $value) {
				if (strpos($route_url, '?')) {
					$route_url .= '&' . $key . '=' . $value;
				} else {
					$route_url .= '?' . $key . '=' . $value;
				}
			}			
		}
		return $route_url;		
	}

}

function route() {
	$CI = &get_instance();
	$CI = $this->routes;
}