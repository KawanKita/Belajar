<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Url_memory {

	protected $CI;	

	public function __construct() {
		$this->CI = & get_instance();
		$this->log();
	}

	public function log() {
		if (!$this->CI->input->is_ajax_request()) {
			$currentURL = $this->CI->session->userdata('currentURL');
			if ($currentURL) {
				$BackURL = $currentURL;
				$this->CI->session->set_userdata('backURL', $BackURL);
			}
			$currentURL = uri_string();
			if ($_SERVER['QUERY_STRING']) {
				$currentURL .= '?' . $_SERVER['QUERY_STRING'];
			}
			$this->CI->session->set_userdata('currentURL', $currentURL);
		}
	}

	public function remember($name, $url) {
		$memory = $this->CI->session->userdata('memory_url');
		$memory[$name] = $url;
		$this->CI->session->set_userdata('memory_url', $memory);
	}

	public function clear($name) {
		$memory = $this->CI->session->userdata('memory_url');
		if (isset($memory[$name])) {
			unset($memory[$name]);
		}
		$this->CI->session->set_userdata('memory_url', $memory);
	}

	public function getURL($name) {
		try {
			$memory = $this->CI->session->userdata('memory_url');
			if (isset($memory[$name])) {
				return $memory[$name];
			} else {
				throw new Exception('MemoryUrlNotFound');				
			}
		} catch(Exception $e) {
			return false;
		}
	}

	public function currentURL($query_string = true) {;
		$currentURL = $this->CI->session->userdata('currentURL');
		if ($query_string) {
			return $currentURL;
		} else {
			$parse = explode('?', $query_string);
			return $parse[0];
		}
	}

	public function backURL($query_string = true) {
		try {
			$backURL = $this->CI->session->userdata('backURL');
			if ($backURL) {
				if ($query_string) {
					return $backURL;
				} else {
					$parse = explode('?', $query_string);
					return $parse[0];
				}				
			} else {
				throw new Exception('BackUrlNotFound');				
			}	
		} catch(Exception $e) {
			show_error($e->getMessage());
		}		
	}

}