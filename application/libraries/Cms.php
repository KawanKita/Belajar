<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms {

    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('comments_model');
        $this->CI->load->model('posts_model');
        $this->CI->load->model('options_model');
    }

    public function comments() {
        return $this->CI->comments_model;
    }

    public function posts() {
        return $this->CI->posts_model;
    }

    public function options() {
        return $this->CI->options_model;
    }

}

function CMS() {
    $CI = &get_instance();
    return $CI->cms;
}