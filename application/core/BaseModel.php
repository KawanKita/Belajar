<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseModel extends CI_Model {

    protected $table;

    protected $primary_key = 'id';    

    protected $fillable = array();

    protected $row_num = 'row_num';

    protected $auto_row_num = false;    

    protected $format_row_num = '{Y}{m}{d}:3';

    protected $set_default = array();

    protected $timestamp = false;

    protected $author = false;

    public function get() {
        return $this->db->get($this->table)->result();
    }

    public function get_by($column, $key) {
        return $this->db->where($column, $key)->get($this->table)->result();        
    }

    public function find($id) {
        $this->db->where($this->primary_key, $id)->get($this->table)->row();
    }

    public function find_by($column, $key) {
        $this->db->where($column, $key)->get($this->table)->row();
    }

    public function insert($record) {
        foreach ($this->set_default as $key => $value) {
            if (!isset($record[$key])) {
                $record[$key] = $value;
            }
        }
        $record = $this->fillable($record);
        if ($this->auto_row_num) {
            $record[$this->row_num] = $this->generate_row_num();
        }
        if ($this->author) {
            $record['created_by'] = getLogin('username');
        }
        if ($this->timestamp) {
            $record['created_at'] = date('Y-m-d');
        }        
        $this->db->insert($this->table, $record);
        $result = $this->find($this->insert_id());
        return $result;
    }

    public function insert_batch($record) {
        foreach ($record as $key => $row) {
            $record[$key] = $this->fillable($record);
            if ($this->author) {
            $record[$key]['created_by'] = getLogin('username');
            }
            if ($this->timestamp) {
                $record[$key]['created_at'] = date('Y-m-d');
            }  
        }
        return $this->db->insert_batch($this->table);
    }

    public function update($id, $column) {
        $record =  $this->fillable($record);
        if ($this->timestamp) {            
            $record['updated_at'] = date('Y-m-d H:i:s');
        }
        if ($this->author) {
            $record['updated_by'] = getLogin('username');
        }
        $this->db->where($this->primary_key, $column)->update($this->table, $record);
        return $this->find($id);
    }

    public function delete($id) {
        return $this->db->where($this->primary_key, $column);
    }

    public function insert_id() {
        return $this->db->insert_id();
    }

    public function fillable($record) {
        $data = array();    
        foreach ($this->fillable as $fillable) { 
            $formatters = array();         
            $parse = explode(':', $fillable);
            if (count($parse) > 1) {
                $fillable = $parse[0];              
                $formatters = explode('|', $parse[1]);
            }          
            if (isset($record[$fillable])) {                   
                if ($record[$fillable]) {                      
                    if (count($formatters) <> 0) {
                        foreach ($formatters as $formatter) {
                            switch ($formatter) {
                                case 'date':
                                    $data[$fillable] = date('Y-m-d', strtotime($record[$fillable]));
                                break;
                                case 'upper':
                                    $data[$fillable] = strtoupper($data[$fillable]);
                                break;
                                case 'lower':
                                    $data[$fillable] = strtolower($data[$fillable]);                        
                                default:
                                    $data[$fillable] = $record[$fillable];
                                break;
                            }                
                        }
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                } else {
                    if ($record[$fillable] === '') {
                        $data[$fillable] = null;
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                }
            }
        }                    
        return $data;
    }

}