<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseModel extends CI_Model {

    protected $table;

    protected $primary_key = 'id';    

    protected $row_num = 'row_num';

    protected $auto_row_num = false;    

    protected $format_row_num = '{Y}{m}{d}:3';

    protected $fillable = array();    

    protected $sortable = null;

    protected $searchable = null;

    protected $set_default = array();

    protected $timestamp = false;

    protected $author = false;

    protected $error = null;

    protected $where = array();

    protected $search = array();

    protected $error_message = null;

    public function get() {        
        $this->_where();
        $this->_search();
        $result = $this->db->get($this->table)->result();                            
        return $result;
    }

    public function get_by($column, $key, $format = true) {
        $this->db->where($column, $key);
        return $this->get($format);
    }

    public function count() {        
        return $this->db->query('select count(*) as total_rows from ('.$this->query().') total_rows')->row()->total_rows;
    }

    public function count_by($column, $key) {
        $this->db->where($column, $key);
        return $this->count();
    }

    public function find($id, $format = true) {
        $this->_where();
        $this->_search();
        $this->db->where('id', $id);
        $result = $this->_find($format);        
        return $result;
    }

    public function find_by($column, $key, $format = true) {
        $this->db->where($column, $key);
        return $this->_find($format);
    }

    public function find_or_fail($id, $format = true) {        
        try {
            if ($result = $this->find($id, $format)) {
                return $result;
            } else {                
                throw new Exception('model-data_not_found');                
            }
        } catch (Exception $e) {                        
            $this->set_error($e->getMessage());
            $this->model_exceptions->find_or_fail();
        }        
    }

    public function find_by_or_fail($column, $key, $format = true) {
        try {
            if ($result = $this->find_by($column, $key, $format)) {
                return $result;
            } else {
                throw new Exception('model-data_not_found');
                
            }
        } catch (Exception $e) {
            $this->set_error($e->getMessage());
            $this->model_exceptions->find_by_or_fail();
        }        
    }

    public function _find() {
        $result = $this->db->get($this->table)->row();                
        return $result;
    }

    public function lists($val_field, $lbl_field, $placeholder = null) {
        $lists = array();
        if ($placeholder) {
            $lists[''] = $placeholder;
        }
        $result = $this->get();
        foreach ($result as $row) {
            $lists[$row->{$val_field}] = $row->$lbl_field;
        }
        return $lists;
    }

    public function query() {
        return $this->db->get_compiled_select($this->table);
    }

    public function where($column, $key = null, $escape = null) {
        $this->where[] = array(
            'column' => $column,
            'key' => $key,
            'escape' => $escape,
            'or' => false
        );
        return $this;
    }

    public function or_where($column, $key = null, $escape = null) {
        $this->where[] = array(
            'column' => $columns,
            'key' => $key,
            'escape' => $escape,
            'or' => true
        );
        return $this;
    }

    public function _where() {
        foreach ($this->where as $where) {
            $column = $where['column'];
            $key = $where['key'];
            $escape = $where['escape'];
            $or = $where['or'];
            if ($or) {
                if (is_array($key)) {
                    $this->db->or_where_in($column, $key, $escape);
                } else {
                    $this->db->or_where($column, $key, $escape);
                }
            } else {
                if (is_array($key)) {
                    $this->db->where_in($column, $key, $escape);
                } else {
                    $this->db->where($column, $key, $escape);
                }
            }
        }
        return $this;
    }    

    public function searchable($columns) {
        $this->searchable = $columns;
        return $this;
    }

    public function sortable($columns) {
        $this->sortable = $columns;
        return $this;
    }

    public function order_by($columns = null, $sort = 'asc') {        
        if ($this->sortable) {
            if (is_array($columns)) {
                foreach ($columns as $column => $sort) {
                    if (in_array($column, $this->sortable)) {
                        $this->db->order_by($column, $sort);
                    }
                }
            } else {                    
                if (in_array($columns, $this->sortable)) {
                    $this->db->order_by($columns, $sort);
                }
            }
        }
        return $this;
    }

    public function search($columns, $key = null) {
        $this->search[] = array(
            'columns' => $columns,
            'key' => $key
        );
        return $this;
    }

    public function _search() {   
        foreach ($this->search as $search) {
            $columns = $search['columns'];
            $key = $search['key'];
            if ($this->searchable) {       
                if (is_array($columns)) {                          
                    if ($key) {                                
                        $this->db->group_start();                        
                        $this->db->like('lower('.$columns[0].')', $key);
                        unset($columns[0]);
                        foreach($columns as $column) {
                            if (in_array($column, $this->searchable)) {
                                $this->db->or_like('lower('.$this->table.'.'.$column.')', strtolower($key));
                            }
                        }
                        $this->db->group_end();
                    } else {
                        $this->db->group_start();                          
                        foreach($columns as $column => $key) {
                            if (in_array($column, $this->searchable)) {
                                $this->db->like('lower('.$this->table.'.'.$column.')', strtolower($key));
                            }
                        }
                        $this->db->group_end();
                    }
                } else {
                    if ($key) {
                        if (in_array($columns, $this->searchable)) {
                            $this->db->like('lower('.$this->table.'.'.$columns.')', strtolower($key));
                        }
                    } else {
                        $key = $columns;
                        if ($key) {
                            $columns = $this->searchable;
                            $this->db->group_start();                        
                            $this->db->or_like('lower('.$this->table.'.'.$columns[0].')', strtolower($key));
                            unset($columns[0]);
                            foreach($columns as $column) {
                                $this->db->or_like('lower('.$this->table.'.'.$column.')', strtolower($key));
                            }
                            $this->db->group_end();
                        } 
                    }
                }
            }
        }        
        return $this;
    }

    public function pagination($size = null, $page = null) {
        $this->pagination = true;               
        if ($this->input->get('page')) {
            $page = $this->input->get('page');
        }
        if ($this->input->get('size')) {
            $size = $this->input->get('size');
        }
        $page = ($page >= 1 || $page) ? $page : 1;
        $size = ($size >= 1 || $size) ? $size : 20;
        $this->db->limit($size, ($page-1) * $size);
        return $this;
    }
    
    public function total_rows() {
        $this->_where();
        $this->_search();
        return $this->count();
    }

    public function insert($record) {        
        foreach ($this->set_default as $key => $value) {
            if (!isset($record[$key])) {
                $record[$key] = $value;
            }
        }
        $record =  $this->fillable($record);                
        if ($this->auto_row_num) {
            $record[$this->row_num] = $this->generate_row_num();
        }
        if ($this->timestamp) {
            $record['created_at'] = date('Y-m-d H:i:s');            
        }
        if ($this->author) {
            $record['created_by'] = getLogin('username');
        }
        try {
            $this->db->trans_begin();
            if (method_exists($this, 'before_insert')) {
                $record = $this->before_insert((array) $record);
            }        
            $result = $this->db->insert($this->table, $record);                    
            if (method_exists($this, 'after_insert')) {
                $this->after_insert($record);
            }
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $id = $this->db->select_max($this->primary_key)->get($this->table)->row()->{$this->primary_key};                
                if ($result) {            
                    return $this->find($id);
                } else {
                    return false;
                }
            } else {
                $this->db->trans_rollback();    
                $this->set_error('model-error_on_db');                
                throw new Exception('model-error_on_db');                
                return false;
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();                    
            return false;
        }     
    }

    public function update($id, $record) {
        $record =  $this->fillable($record);
        if ($this->timestamp) {            
            $record['updated_at'] = date('Y-m-d H:i:s');
        }
        if ($this->author) {
            $record['updated_by'] = getLogin('username');
        }
        $old = (array) $this->find($id, false);   
        $new = $old;
        foreach ($record as $key => $val) {
            $new[$key] = $val;
        }
        try {
            $this->db->trans_begin();
            if (method_exists($this, 'before_update')) {                                    
                $record = $this->before_update($old, $new);
            }
            $result = $this->db->where('id', $id)->update($this->table, $record);
            if (method_exists($this, 'after_update')) {
                $this->after_update($old, $new);
            }
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                if ($result) {
                    return $this->find($id);
                } else {
                    return false;
                }
            } else {
                $this->db->trans_rollback();  
                $this->set_error('model-error_on_db');                                
                throw new Exception('model-error_on_db');                  
                return false;
            }            
        } catch (Exception $e) {
            $this->db->trans_rollback();                  
            return false;
        }        
    }

    public function delete($id) {
        try {
            $this->db->trans_begin();
            $old = (array) $this->find($id, false);          
            if (method_exists($this, 'before_delete')) {                                    
                $old = $this->before_delete($old);
            }     
            $result = $this->db->where('id', $id)->delete($this->table);
            if (method_exists($this, 'after_delete')) {                                    
                $old = $this->after_delete($old);
            }
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return $result;
            } else {
                $this->db->trans_rollback();    
                $this->set_error('model-error_on_db');       
                throw new Exception('model-error_on_db');      
                return false;
            } 
        } catch (Exception $e) {
            $this->db->trans_rollback();                       
            return false;
        }        
    }    

    public function fillable($record) {
        $data = array();    
        foreach ($this->fillable as $fillable) { 
            $formatters = array();         
            $parse = explode(':', $fillable);
            if (count($parse) > 1) {
                $fillable = $parse[0];              
                $formatters = explode('|', $parse[1]);
            }          
            if (isset($record[$fillable])) {                   
                if ($record[$fillable]) {                      
                    if (count($formatters) <> 0) {
                        foreach ($formatters as $formatter) {
                            switch ($formatter) {
                                case 'date':
                                    $data[$fillable] = date('Y-m-d', strtotime($record[$fillable]));
                                break;
                                case 'upper':
                                    $data[$fillable] = strtoupper($data[$fillable]);
                                break;
                                case 'lower':
                                    $data[$fillable] = strtolower($data[$fillable]);                        
                                default:
                                    $data[$fillable] = $record[$fillable];
                                break;
                            }                
                        }
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                } else {
                    if ($record[$fillable] === '') {
                        $data[$fillable] = null;
                    } else {
                        $data[$fillable] = $record[$fillable];
                    }
                }
            }
        }                    
        return $data;
    }

    public function generate_row_num() {
        $format = $this->format_row_num;
        $parse = explode(':', $format);
        $prefix = str_replace(array('{Y}', '{m}', '{d}'), array(date('Y'), date('m'), date('d')), $parse[0]);
        $digit = str_repeat('0', $parse[1]);
        $last_id =  $this->db->select_max($this->row_num)
        ->where('left('.$this->row_num.','.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->{$this->row_num};
        if ($last_id) {            
            $counter = substr($last_id, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }        
    }

    public function set_error($key, $message = null) {
        $this->error = $key;
        if ($message) {
            $this->error_message = $message;
        } else {
            if (lang($this->error)) {
                $this->error_message = lang ($this->error);
            }
        }        
    }

    public function get_error() {
        return $this->error;
    }

    public function get_error_message() {
        return $this->error_message;
    }

}