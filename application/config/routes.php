<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['search'] = 'posts/search';
$route['search/load_more'] = 'posts/search_load_more';

$route['berita'] = 'posts/archive/news';
$route['berita/(:num)'] = 'posts/archive/news/$1';
$route['berita/(:num)/(:num)'] = 'posts/archive/news/$1/$2';
$route['berita/(:num)/(:num)'] = 'posts/archive/news/$1/$2';
$route['berita/(:num)/(:num)/(:any)'] = 'posts/archive/news/$1/$2/$3';

$route['pengumuman'] = 'posts/archive/info';
$route['pengumuman/(:num)'] = 'posts/archive/info/$1';
$route['pengumuman/(:num)/(:num)'] = 'posts/archive/info/$1/$2';
$route['pengumuman/(:num)/(:num)'] = 'posts/archive/info/$1/$2';
$route['pengumuman/(:num)/(:num)/(:any)'] = 'posts/archive/info/$1/$2/$3';


$route['galeri'] = 'posts/archive/gallery';
$route['galeri/(:num)'] = 'posts/archive/gallery/$1';
$route['galeri/(:num)/(:num)'] = 'posts/archive/gallery/$1/$2';
$route['galeri/(:num)/(:num)'] = 'posts/archive/gallery/$1/$2';
$route['galeri/(:num)/(:num)/(:any)'] = 'posts/archive/gallery/$1/$2/$3';

$route['archive/detail/(:any)'] = 'posts/archive_detail/$1';

$route['pengaduan'] = 'comments/comment';
$route['pengaduan/load_more'] = 'comments/comment_load_more';
$route['pengaduan/send'] = 'comments/send/pengaduan';

$route['paket_siap_lelang'] = 'home/paket_siap_lelang';
$route['proses_lelang'] = 'home/proses_lelang';

$route['(:any)'] = 'posts/page/$1';

$config['route_name'] = array(
	'cms' => 'cms'
);
