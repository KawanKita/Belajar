<?php

$config = array(

	'post_type' => array(
		'news' => 'Berita',
		'info' => 'Pengumuman',
		'page' => 'Page',
		'gallery' => 'Galeri'
	),

	'post_type_url' => array(
		'news' => 'berita',
		'info' => 'pengumuman',
		'page' => 'page',
		'gallery' => 'galeri'
	),

	'post_type_allowed_search' => array('news', 'info', 'gallery'),

	'short_month' => array(
		'01' => 'JAN`',
		'02' => 'FEB',
		'03' => 'MAR',
		'04' => 'APR',
		'05' => 'MEI',
		'06' => 'JUN',
		'07' => 'JUL',
		'08' => 'AGS',
		'09' => 'SEP',
		'10' => 'OKT',
		'11' => 'NOV',
		'12' => 'DESC'
	),

);