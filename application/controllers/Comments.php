<?php
/**
 * Created by PhpStorm.
 * User: Av
 * Date: 19/10/2016
 * Time: 17:48
 */

class Comments extends CI_Controller {

	public function comment() {
		$this->load->view('page/pengaduan/comment');
	}

	public function comment_load_more() {
		$this->load->view('page/pengaduan/comment_load_more');
	}

    public function send($type) {
        $post = $this->input->post();

        $create = CMS()->comments()->create($type, $post);

        if ($create) {
            $this->response->generate(true, 0);
        } else {
            $this->response->generate(false, 201, 'Pengaduan Anda gagal dikirim.');
        }
    }

}