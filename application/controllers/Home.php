<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $this->load->view('index');
    }

    public function paket_siap_lelang() {
    	$this->load->view('page/paket_siap_lelang/paket_siap_lelang');
    }

    public function proses_lelang() {
    	$this->load->view('page/proses_lelang/proses_lelang');
    }
}
