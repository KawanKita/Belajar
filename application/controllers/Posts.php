<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Av
 * Date: 18/10/2016
 * Time: 17:59
 */

class Posts extends CI_Controller {

	public function search() {
		$this->load->view('page/posts/search');
	}

	public function search_load_more() {
		$this->load->view('page/posts/search_load_more');
	}

	public function archive($type = null, $year = null, $month = null, $slug = null) {				
		if (!$year) {
			$year = date('Y');
		}
		if (!$month) {
			$month = date('m');
		}		
		$this->load->view('page/posts/archive', array(
			'type' => $type,
			'year' => $year,
			'month' => $month,
			'slug' => $slug
		));
	}

	public function archive_detail($slug) {
		$this->load->view('page/posts/archive_detail', array(
			'slug' => $slug
		));
	}

	public function page($slug) {
		$this->load->view('page/posts/page', array(
			'slug' => $slug
		));
	}

    public function detail($slug = '') {
    	$this->load->view('page/posts/detail');
    }
 
}