<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts_m extends BaseModel {

	protected $table = 'cms_posts';
	protected $fillable = array('title', 'post_date:date', 'content', 'main_image', 'format', 'type', 'status');
	protected $author = true;
	protected $timestamp = true;

	public function __construct() {
		parent::__construct();
		$this->set_default = array(
			'post_date' => date('Y-m-d'),
			'status' => 'draft'
		);
	}

	public function get_post($archive = null, $status = '',  $category = null, $tag = null) {
		if ($archive) {
			$this->db->where('left(cms_posts.post_date, 7) = ', $archive);
		}

		if ($status) {
			$this->db->where('cms.status', $status);
		}

		if ($category) {
			$this->db->like('cms_categories.tree_id', $category, 'right');
		}

		if ($tag) {
			$this->db->where('cms_post_tags.tag_id', $tag);
		}

		return $this->db->select('cms_posts.*, cms_slugs.slug')	
		->join('cms_slugs', 'cms_slugs.relation_id = cms_posts.id and cms_slugs.type = "post"')
		->join('cms_post_categories', 'cms_post_categories.post_id = cms.posts.id')
		->join('cms_categories', 'cms_categories.id = cms_post_categories.category_id')
		->join('cms_post_tags', 'cms_post_tags.post_id = cms_posts.id')
		->join('cms_tags', 'cms_tags.id = cms_post_tags.tag_id')
		->group_by('cms_posts.id')
		->get('cms_posts')
		->result();
	}	

	public function find_post($id) {
		$this->db->select('cms_posts.*, cms_slugs.slug')
		->join('cms_slugs', 'cms_slugs.relation_id = cms_posts.id and cms_slugs.type = "post"')
		->where('cms_posts.id', $id)
		->get('cms_posts')
		->row();
	}

	public function get_categories($id) {
		return $this->db->where('post_id', $id)->get('cms_post_categories')->result();
	}

	public function get_tags($id) {
		return $this->db->where('post_id', $id)->get('cms_post_tags')->result();
	}

	public function get_images($id) {
		return $this->db->where('post_id', $id)->get('cms_post_images')->result();
	}

	public function get_options($id) {
		return $this->db->where('post_id', $id)->get('cms_post_options')->result();
	}

	public function insert_post($record) {		
		$post = $this->insert($this->fillable($record));
		$this->db->insert('cms_slugs', array(
			'type' => 'post',
			'slug' => $record['slug'],
			'relation_id' => $post->id
		));
		if (isset($record['categories'])) {
			if (count($record['categories']) > 0) {
				$record_categories = array();
				foreach ($record['categories'] as $category) {
					$record_categories[] = array(
						'post_id' => $post->id,
						'category_id' => $category
					);
				}
				$this->db->insert_batch('cms_post_categories', $record_categories);
			}
		}
		if (isset($record['tags'])) {
			if (count($record['tags']) > 0) {
				$record_tags =array();
				foreach ($record['tags'] as $tag) {
					$record_tags[] = array(
						'post_id' => $post->id,
						'tag_id' => $tag
					);
				}
				$this->db->insert_batch('cms_post_tags', $record_tags);
			}
		}
		if (isset($record['images'])) {
			if (count($record['images']) > 0) {
				$record_images = array();
				foreach ($record['images'] as $image) {
					$record_images[] = array(
						'post_id' => $post->id,
						'image_url' => $image['image_url'],
						'caption' => $image['caption'],
						'link' => $image['link']
					);
				}
				$this->db->insert_batch('cms_post_images', $record_images);
			}
		}
		if (isset($record['options'])) {
			if (count($record['options']) > 0) {
				$record_options = array();
				foreach ($record['options'] as $key => $option) {
					$record_options[] = array(
						'post_id' => $post->id,
						'key' => $key,
						'value' => $option
					);
				}
				$this->db->insert_batch('cms_post_options', $record_options);
			}
		}
		return $post;
	}
}