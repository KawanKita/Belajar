<?php
/**
 * Created by PhpStorm.
 * User: Av
 * Date: 18/10/2016
 * Time: 20:58
 */

class Comments_model extends CI_Model {

    public function search($key, $limit, $offset) {
        $parse = explode(' ', $key);
        $this->db->group_start();       
        $this->db->like('a.comment', $parse[0])
        ->or_like('a.email', $parse[0]);
        unset($parse[0]);
        if (count($parse) > 0) {                            
            foreach ($parse as $word) {
                $this->db->or_like('a.comment', $word)
                ->or_like('a.email', $word);
            }            
        }
        $this->db->group_end();
        $result = $this->db->where('a.status', 'appr')
            ->order_by('a.id', 'DESC')
            ->get('cms_comments a', $limit, $offset);
        if ($limit > 1) {
            return $result->result();
        } else {
            return $result->row();
        }
    }

    public function latest($type, $limit, $offset) {
        $result = $this->db->where('parent_id', null)
            ->where('type', $type)
            ->where('status', 'appr')
            ->order_by('id', 'DESC')
            ->get('cms_comments', $limit, $offset);
        if ($limit > 1) {
            return $result->result();
        } else {
            return $result->row();
        }
    }

    public function reply($parent_id, $type, $limit, $offset) {
        $result = $this->db->where('parent_id', $parent_id)
            ->where('type', $type)
            ->order_by('id', 'DESC')
            ->get('cms_comments', $limit, $offset);
        if ($limit > 1) {
            return $result->result();
        } else {
            return $result->row();
        }
    }

    public function create($type, $post) {
        $record = array(
            'comment_date' => date('Y-m-d'),
            'type' => $type,
            'name' => $post['email'],
            'email' => $post['email'],
            'comment' => $post['comment'],
            'status' => 'wappr',
            'is_admin' => 'false',
            'created_by' => $post['email'],
            'created_at' => date('Y-m-d')
        );
        return $this->db->insert('cms_comments', $record);
    }

}