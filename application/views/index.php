<?php section('content') ?>

	<!-- Banner Slide Show -->
    <?php
        $theme_main_slider = CMS()->options()->get('theme_main_slider');
        if ($theme_main_slider) {
            ?>
            <div id="carousel-example-generic" style="margin-top: -25px;" class="carousel slide hidden-sm hidden-xs" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $main_slider = CMS()->posts()->post_detail($theme_main_slider);
                    $counter = 1;
                    foreach ($main_slider->images as $row) {
                        ?>
                        <div class="item <?= ($counter == 1) ? 'active' : '' ?>">
                            <img src="<?= $row->image_url ?>">

                            <div class="carousel-caption">
                                <div class="caption-slider">
                                    <div class="caption-date"><?= $row->caption ?></div>
                                    <div class="caption-description"><?= content_limiter($row->description, 100) ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $counter++;
                    }
                    ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <?php
        }
    ?>

    <div class="content-menu hidden-xs">
        <div class="container" style="margin-top: 10px;">
            <div class="col-sm-3 content-menu-m" style="border-right: 2px solid #ddd;">
                <div class="form-group">
                    <div class="col-md-5 col-sm-12 col-xs-12 content-image">
                        <img class="img-responsive" src="<?= base_url('public/img/image-item-1.jpg')?>">
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12 content-title">
                        Management Dokumen Arsip
                    </div>
                </div>
            </div>
            <div class="col-sm-3 content-menu-m">
                <div class="form-group">
                    <div class="col-md-5 col-sm-12 col-xs-12 content-image">
                        <img class="img-responsive" src="<?= base_url('public/img/image-item-2.jpg')?>">
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12 content-title">
                        Monitoring Evaluasi & Laporan 
                    </div>
                </div>
            </div>
            <div class="col-sm-3 content-menu-m" style="border-left: 2px solid #ddd; border-right: 2px solid #ddd;">
                <div class="form-group">
                    <div class="col-md-5 col-sm-12 col-xs-12 content-image">
                        <img class="img-responsive" src="<?= base_url('public/img/image-item-3.jpg')?>">
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12 content-title">
                        <a href="http://lpse.jatimprov.go.id/eproc" target="_blank">LPSE<br>Prov. Jatim</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 content-menu-m">
                <div class="form-group">
                    <div class="col-md-5 col-sm-12 col-xs-12 content-image">
                        <img class="img-responsive" src="<?= base_url('public/img/image-item-4.jpg')?>">
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12 content-title">
                        <a href="http://119.252.169.165" target="_blank" class="text-default">Apel Baja<br>2016</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="content-body">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12 side-car">
					<div class="bunder">
						<div class="label-chart">
							<h3>Progres</h3>
							<h1>Pengadaan</h1>
						</div>
						<div class="donut-chart">
							<canvas id="pieChart" height="250"></canvas>
						</div>
					</div>
					<div class="bar">
						<div class="segitiga"></div>
						<div class="label-chart-bar">
							<h3>Pengadaan</h3>
							<h1>Barang</h1>
						</div>
						<div class="bar-chart">
							<div id="bar-chart" style="height: 300px;"></div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-12 hot-news">
							<div class="form-group">
								<div class="title-text-pengumuman">
									<a href=<?= base_url('pengumuman') ?>>PENGUMUMAN</a>
								</div>

                                <?php
                                    $latest_info = CMS()->posts()->latest('info', 1, 0);
                                    if ($latest_info) {
                                        ?>
                                        <div class="col-md-6 hot-news-left">
                                            <ul>
                                                <li>
                                                    <a href="<?= base_url('pengumuman/'.post_archive($latest_info->post_date).'/'.$latest_info->slug) ?>">
                                                        <p><?= content_limiter($latest_info->title, 50) ?></p>

                                                        <p style="font-size: 0.7em;"><?= locale_human_date($latest_info->post_date) ?></p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-6 hot-news-left">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <p>Belum ada pengumuman</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php
                                    }
                                ?>

								<div class="col-md-6 hot-news-right">
									<ul>
                                        <?php
                                            $more_info = CMS()->posts()->latest('info', 3, 1);
                                            if ($more_info) {
                                                foreach($more_info as $row) {
                                                    ?>
                                                    <li><a href="<?= base_url('pengumuman/'.post_archive($row->post_date).'/'. $row->slug) ?>"><?= $row->title ?></a></li>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <li><a href="#">Belum ada pengumuman</a></li>
                                                <?php
                                            }
                                        ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- For lg size -->
						<div class="col-md-12 news hidden-md hidden-sm hidden-xs">
							<div class="form-group">
								<div class="title-text"><a href=<?= base_url('berita') ?>>BERITA</a></div>
                                <?php
                                    $latest_news = CMS()->posts()->latest('news', 1, 0);
                                    if ($latest_news) {
                                        ?>
                                        <div class="col-md-7 news-top-img">
                                            <img src="<?= $latest_news->main_image ?>" class="img-responsive" style="margin-left: -30px;">
                                        </div>
                                        <div class="col-md-5 news-top-text">
                                            <div class="news-top-text-date"><?= locale_human_date($latest_news->post_date) ?></div>
                                            <div class="news-top-text-data">
                                                <?= content_limiter($latest_news->title, 50) ?>
                                                <a style="width:100px;" class="button -white" type="button" href="<?= base_url('berita/'.post_archive($latest_news->post_date).'/'.$latest_news->slug) ?>">BACA</a>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col-md-7 news-top-img news-top-text">
                                            <div class="news-top-text-data">
                                                <i>( *Tidak ada gambar ditampilkan )</i>
                                            </div>
                                        </div>
                                        <div class="col-md-5 news-top-text">
                                            <div class="news-top-text-data">
                                                Belum ada berita
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
							</div>
						</div>

						<div class="col-md-12 news-bottom">
							<div class="news-bottom-title">
								<span class="grey">BERITA LAINNYA</span>
								<span class="grey pull-right"><a href="<?= base_url('berita') ?>">ARSIP BERITA <i class="fa fa-arrow-right"></i></a></span>
							</div>
							<div class="form-group">
								<div class="news-bottom-content">
                                    <?php
                                        $more_news = CMS()->posts()->latest('news', 3, 1);
                                        if ($more_news) {
                                            foreach ($more_news as $row) {
                                                ?>
                                                <div class="col-md-4">
                                                    <div class="news-bottom-box">
                                                        <img src="<?= $row->main_image ?>" class="img-responsive">

                                                        <div class="news-bottom-box-date"><?= locale_human_date($row->post_date) ?></div>
                                                        <div class="news-bottom-box-data">
                                                            <a href="<?= base_url('berita/'.post_archive($row->post_date).'/'.$row->slug) ?>"><?= content_limiter($row->title, 50) ?><br/><i style="font-size: 0.8em;">(lanjut membaca)</i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="col-md-4">
                                                <div class="news-bottom-box">
                                                    <div class="news-bottom-box-data">
                                                        <a href="#">Belum ada berita</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- End Row -->

			<div class="row">
				<!-- For lg md sm size -->
				<div class="col-md-8 col-sm-12 col-xs-12 complaint hidden-xs">
					<div class="title-text-complaint margin-min"><a href="<?= base_url('pengaduan'); ?>">PENGADUAN</a></div>
					<div class="news-complaint-box">
						<div class="form-group">
                            <?php
                                $latest_comment = CMS()->comments()->latest('pengaduan', 3, 0);
                                if ($latest_comment) {
                                    foreach ($latest_comment as $row) {
                                        ?>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="news-complaint">
                                                <div class="news-bottom-box-date"><?= $row->email ?></div>
                                                <div class="news-complaint-text">
                                                    <?= content_limiter($row->comment, 100) ?>
                                                </div>
                                                <div class="news-complaint-answer">Jawaban :</div>
                                                <?php $reply = CMS()->comments()->reply($row->id, 'pengaduan', 1, 0); ?>
                                                <div class="news-complaint-text-answer">
                                                    <?= ($reply) ? content_limiter($reply->comment, 100) : 'Belum terjawab.' ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="news-complaint">
                                            <div class="news-complaint-text">
                                                <i>( *Belum ada pengaduan ditampilkan )</i>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
						</div>
					</div>
				</div>
				<!-- End -->

				<div class="col-md-4 col-sm-12 col-xs-12 form-complaint">
					<div class="form-group">
						<div class="form-complaint-title">
							Punya keluhan soal proses lelang ? Laporkan pada kami disini!
						</div>
						<div class="form-complaint-input">
							<form class="form" role="form" method="post" enctype="multipart/form-data" id="pengaduan">
								<p class="email">
									<input name="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="email" />
								</p>

								<p class="text">
									<textarea name="comment" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="sampaikan keluhan disini..."></textarea>
								</p>

								<button type="submit" class="button -red pull-right">KIRIM</button>
							</form>
						</div>
					</div>
				</div>
			</div>
            <?php
                $theme_main_galery = CMS()->options()->get('theme_main_gallery');
                if ($theme_main_galery) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="gallery-info">
                                <div class="title-text-galeri"><a href="<?= base_url('galeri') ?>">GALERI</a></div>
                                <div class="gallery-info-photo">
                                    <ul class="pgwSlideshow">
                                        <?php
                                        $gallery = CMS()->posts()->post_detail($theme_main_galery);
                                        $counter = 1;
                                        foreach ($gallery->images as $row) {
                                            ?>
                                            <li>
                                                <img src="<?= $row->image_url ?>" alt="<?= $row->caption ?>" data-description="<?= content_limiter($row->description, 100) ?>">
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            ?>
		</div>
	</div>

    <footer class="footer-i hidden-sm hidden-xs">
        <div class="container">
            <div class="col-md-12">
                &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
            </div>
        </div>
    </footer>

<?php endsection() ?>

<?php section('page_script') ?>
    <script>
        $(function() {
            $('#pengaduan').submit(function() {
                $.ajax({
                    url : base_url('pengaduan/send'),
                    type : 'post',
                    dataType : 'json',
                    data : $('#pengaduan').serialize(),
                    success :function(response) {
                        if (response['success']) {
                            alert('Pengaduan Anda telah terkirim.')

                            $('#email').val('');
                            $('#comment').val('');
                        } else {
                            if (response['status'] == 101) {
                                $('#pengaduan').submit();
                            } else {
                                alert(response['data']);
                            }
                        }
                    }
                });

                return false;
            });
        });
    </script>
    <script>
        $(function () {
            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = [
                {
                    value: 700,
                    color: "#f56954",
                    highlight: "#f56954",
                    label: "Barang"
                },
                {
                    value: 500,
                    color: "#00a65a",
                    highlight: "#00a65a",
                    label: "Jasa Konsultasi"
                },
                {
                    value: 400,
                    color: "#f39c12",
                    highlight: "#f39c12",
                    label: "Jasa Konstruksi"
                },
                {
                    value: 600,
                    color: "#00c0ef",
                    highlight: "#00c0ef",
                    label: "Jasa Lainya"
                }
            ];
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: false,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
        });
    </script>

    <!-- Page script -->
    <script type="text/javascript">

        $(function () {
            /*
             * BAR CHART
             * ---------
             */

            var bar_data = {
                data: [["Pengajuan <h1>(24)</h1>", 24], ["Bermasalah <h1>(3)</h1>", 3], ["Terverifikasi <h1>(15)</h1>", 15], ["Selesai <h1>(19)</h1>", 19]],
                color: "#18BCBE"
            };
            $.plot("#bar-chart", [bar_data], {
                grid: {
                    borderWidth: 1,
                    borderColor: "#f3f3f3",
                    tickColor: "#f3f3f3"
                },
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.8,
                        align: "center"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });
            /* END BAR CHART */
        });

        /*
         * Custom Label formatter
         * ----------------------
         */
        function labelFormatter(label, series) {
            return "<div style='font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;'>"
                + label
                + "<br/>"
                + Math.round(series.percent) + "%</div>";
        }

        $(document).ready(function() {
            $('.pgwSlideshow').pgwSlideshow();
        });
    </script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>