<div class="error">
	<div class="container">
		<div class="type-error">Error 404</div>
		<div class="error-description">Mohon maaf halaman<br> yang Anda cari tidak<br> ditemukan</div>
		<div class="back"><a href="<?= base_url() ?>"><i class="ion-arrow-left-c"></i> kembali</a></div>
	</div>
</div>

<footer class="footer hidden-sm hidden-xs">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>