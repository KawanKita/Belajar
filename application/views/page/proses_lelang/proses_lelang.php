<?php section('content'); ?>

	<div class="container-fluid">
		<div class="content-body">
			<div class="container">
				<div class="title-form light-grey margin-plus">Daftar Proses Lelang</div>
				<div class="auction-info">
					<div class="auction-info-search">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<input name="name" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Masukkan kode atau nama lelang" id="input-search-p" />
							</div>
							<div class="col-md-6 col-sm-12 input-select-i">
								<label class="col-md-5 control-label light-grey letter-spacing text-right">JENIS PEKERJAAN</label>
					            <div class="col-md-7 select-dropdown" data-settings='{"wrapperClass":"metro"}'>
					             	<select class="dropdown">
									    <option value="" class="label">Semua</option>
									    <option value="1">Option 1</option>
									    <option value="2">Option 2</option>
									    <option value="3">Option 3</option>
									    <option value="4">Option 4</option>
									    <option value="5">Option 5</option>
									</select> 
					            </div>
							</div>
						</div>
					</div>
					<div style="width:auto; overflow-x: auto;">
						<div style="width: 1125px;">
							<p class="hr"></p>
						    <div class="row">
								<div class="col-xs-5">
									<div class="light-grey letter-spacing">NAMA LELANG</div>
								</div>
								<div class="col-xs-2">
									<div class="light-grey letter-spacing">JENIS PEKERJAAN</div>
								</div>
								<div class="col-xs-3">
									<div class="light-grey letter-spacing">SKPD</div>
								</div>
								<div class="col-xs-2">
									<div class="light-grey letter-spacing">HPS</div>
								</div>
							</div>
							<p class="border-bottom-table"></p>

							<!-- Repeat proses lelang -->
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Pembangunan/ rehabilitasi sarana dan prasarana penyediaan air bersih
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Barang/Jasa</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<!-- End Repeat -->

							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Pembangunan pengerukan, dermaga, reventment, reklamasi, rigid beton, dan groin di PPI Tambakrejo Kabupaten Blitar
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Konstruksi</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>DISHUB & LLAJ</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Perencanaan Pembangunan Gedung IGD di RS Paru Dungus Madiun
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Barang/Jasa</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Pengadaan dan pemasangan fasilitas keselamatan jalan
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Jasa Konsultasi</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Penunjangan Jalan di Jalan Jurs.Pandaan-Tretes (Link 187) Km. Surabaya : 50+000-53+000
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Barang/Jasa</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Perencanaan Pembangunan ATCS di Kab. Sidoarjo dan Kediri
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Jasa Lainnya</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Pengadaan dan pemasangan fasilitas keselamatan jalan
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Barang/Jasa</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<div class="row">
								<div class="col-xs-5">
									<p class="paket-siap-lelang-title light-grey">
										Pembangunan/ rehabilitasi sarana dan Prasarana Penyediaan Air Bersih
									</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>Konstruksi</p>
								</div>
								<div class="col-xs-3 light-grey">
									<p>Dinas Pekerjaan Umum Cipta Karya</p>
								</div>
								<div class="col-xs-2 light-grey">
									<p>120,347,000,000</p>
								</div>
							</div>
							<p class="hr"></p>
							<p>
						    	<button type="button" class="button -greywhite">LOAD MORE</button>
						    </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<footer class="footer hidden-sm hidden-xs">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<?php endsection(); ?>

<?php getview('layouts/layout'); ?>