<?php
	$page = $this->input->get('page');
	$page = !is_numeric($page) ? 1 : $page;
	$key = $this->input->get('key');
	$per_page = 8;
	$posts = CMS()->posts()->search($this->input->get('key'), $per_page, ($page-1) * $per_page);
?>
<?php foreach($posts as $row) { ?>
    <div class="row">    
        <?php if($row->main_image) { ?>
            <div class="col-xs-3">
                <div class="search-image">
                    <img class="img-responsive" src="../../dist/img/image-gallery.jpg" />
                </div>
            </div>
            <div class="col-xs-7 light-grey">
                <a href="<?= base_url(getConfig('row_type_url')[$row->type] .'/'.post_archive($row->post_date).'/'.$row->slug) ?>" class="search-view-title red"><?= $row->title ?></a>
                <p>
                    <?= content_limiter($row->content, 500) ?>
                </p>
            </div>
            <div class="col-xs-2 text-center light-grey">
                <p class="red"><?= getConfig('row_type')[$row->type] ?></p>
            </div>                
        <?php } else { ?>                        
            <div class="col-xs-10 light-grey">
                <a href="<?= base_url(getConfig('row_type_url')[$row->type] .'/'.post_archive($row->post_date).'/'.$row->slug) ?>" class="search-view-title red"><?= $row->title ?></a>
                <p>
                    <?= content_limiter($row->content, 500) ?>
                </p>
            </div>
            <div class="col-xs-2 text-center light-grey">
                <p class="red"><?= getConfig('row_type')[$row->type] ?></p>
            </div>                
        <?php } ?>
    </div>        
    <p class="hr"></p>    
<?php } ?>
<?php if ($posts) { ?>
    <button type="button" id="btn_more" name="btn_more" class="button -greywhite" onclick="loadMore()">LOAD MORE</button>        
<?php } ?>