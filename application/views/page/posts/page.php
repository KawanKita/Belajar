<?php section('content') ?>
    <?php $post = CMS()->posts()->post_detail_by_slug($slug) ?>
    <?php if($post) { ?>
        <?php
            if ($post->format == 'image') {
                section('content');
                ?>
                <div class="container">
                    <div class="gallery-info">
                        <div class="gallery-info-photo">
                            <ul class="pgwSlideshow">
                                <?php
                                $gallery = CMS()->posts()->post_detail($post->id);
                                $counter = 1;
                                foreach ($gallery->images as $row) {
                                    ?>
                                    <li>
                                        <img src="<?= $row->image_url ?>" alt="<?= $row->caption ?>" data-description="<?= content_limiter($row->description, 100) ?>">
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <footer class="footer hidden-sm hidden-xs">
                    <div class="container">
                        <div class="col-md-12">
                            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
                        </div>
                    </div>
                </footer>

                <footer class="footer-xs hidden-lg hidden-md">
                    <div class="container">
                        <div class="col-md-12">
                            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
                        </div>
                    </div>
                </footer>
                <?php
                endsection();

                section('page_script');
                ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.pgwSlideshow').pgwSlideshow();
                    });
                </script>
                <?php
                endsection();
            } else {
                section('content');
            ?>
            <div class="container">
                <?php
                    if ($post->main_image) { 
                ?>
                        <div class="image-galeri-berita">
                            <center><img style="max-width:400px; max-height: 270px;" src="<?= $post->main_image ?>"></center>
                        </div>
                    <?php } ?>
                    <div class="title-galeri-berita">
                        <p class="tgl-title-berita"><?= locale_human_date($post->post_date) ?></p>
                        <label class="title-h-berita"><?= $post->title ?></label>

                        <div class="content-galeri-berita">
                            <?= $post->content ?>
                        </div>
                    </div>
                    <footer class="footer hidden-sm hidden-xs">
                        <div class="container">
                            <div class="col-md-12">
                                &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
                            </div>
                        </div>
                    </footer>

                    <footer class="footer-xs hidden-lg hidden-md">
                        <div class="container">
                            <div class="col-md-12">
                                &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
                            </div>
                        </div>
                    </footer>
                </div>
                <?php
                    endsection();
                }
        ?>
    <?php
        } else {
        ?>
        <?php section('content') ?>
            <?= getview('404') ?>
        <?php endsection() ?>
        <?php
    }
    ?>
    
<?php getview('layouts/layout'); ?>