<?php section('content') ?>
<div class="content-body">
    <div id="container" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-form grey-c" style="font-size: 2em;">Pencarian</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <form>
                    <p class="name input-search" style="margin-top: 50px;">
                    <input name="key" type="text" value="<?= $this->input->get('key') ?>" class="feedback-input" placeholder="Masukkan kata kunci pencarian" id="input-search-p" />
                    </p>
                </form>
            </div>
        </div>
        <p class="hr"></p> 
        <!-- Search yang ada gambarnya -->                     
        <?php $posts = CMS()->posts()->search($this->input->get('key'), 8, 0) ?>
        <?php if(count($posts) > 0) { ?>
            <?php foreach($posts as $post) { ?>
                <div class="row">    
                    <?php if($post->main_image) { ?>
                        <div class="col-xs-3">
                            <div class="search-image">
                                <img class="img-responsive" src="<?= $post->main_image; ?>" />
                            </div>
                        </div>
                        <div class="col-xs-7 light-grey">
                            <a href="<?= base_url(getConfig('post_type_url')[$post->type] .'/'.post_archive($post->post_date).'/'.$post->slug) ?>" class="search-view-title red"><?= $post->title ?></a>
                            <p>
                                <?= content_limiter($post->content, 500) ?>
                            </p>
                        </div>
                        <div class="col-xs-2 text-center light-grey">
                            <p class="red"><?= getConfig('post_type')[$post->type] ?></p>
                        </div>                
                    <?php } else { ?>                        
                        <div class="col-xs-10 light-grey">
                            <a href="<?= base_url(getConfig('post_type_url')[$post->type] .'/'.post_archive($post->post_date).'/'.$post->slug) ?>" class="search-view-title red"><?= $post->title ?></a>
                            <p>
                                <?= content_limiter($post->content, 500) ?>
                            </p>
                        </div>
                        <div class="col-xs-2 text-center light-grey">
                            <p class="red"><?= getConfig('post_type')[$post->type] ?></p>
                        </div>                
                    <?php } ?>
                </div>        
                <p class="hr"></p>
            <?php } ?>            
        <?php } else { ?>
            <div class="row">
                <div class="col-xs-12">
                    <span class="search-view-title red">Tidak ada halaman yang ditemukan.</span>
                </div>
            </div>
            <p class="hr"></p>
        <?php } ?>          
        <button type="button" id="btn_more" name="btn_more" class="button -greywhite" onclick="loadMore()">LOAD MORE</button>        
    </div>
</div>


<footer class="footer hidden-sm hidden-xs">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>
<?php endsection() ?>
<?php section('page_script') ?>
<script>
    var page = 1;
    function loadMore() {
        page++;
        $('#btn_more').html('LOADING...').attr('disabled', true);
        $.ajax({
            url : '<?= base_url('search/load_more?key=' . $this->input->get('key')) ?>&page=' + page,
            success :function(response) {
                $('#btn_more').remove();
                $('#container').append(response);
            }
        });
    }
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>