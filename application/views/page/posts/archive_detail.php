<?php section('content') ?>
    <?php $post = CMS()->posts()->post_detail_by_slug($slug) ?>
    <?php if($post) { ?>
        <?php
            if ($post->format == 'image') {
                section('content');
                ?>
                <div class="gallery-info">
                    <div class="gallery-info-photo">
                        <ul class="pgwSlideshow">
                            <?php
                            $gallery = CMS()->posts()->post_detail($post->id);
                            $counter = 1;
                            foreach ($gallery->images as $row) {
                                ?>
                                <li>
                                    <img src="<?= $row->image_url ?>" alt="<?= $row->caption ?>" data-description="<?= content_limiter($row->description, 100) ?>">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php
                endsection();

                section('page_script');
                ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.pgwSlideshow').pgwSlideshow();
                    });
                </script>
                <?php
                endsection();
            } else {
                section('content');

                if ($post->main_image) { ?>
                    <div class="image-galeri-berita">
                        <center><img style="max-width:400px; max-height: 270px;" src="<?= $post->main_image ?>"></center>
                    </div>
                <?php } ?>
                <div class="title-galeri-berita">
                    <p class="tgl-title-berita"><?= locale_human_date($post->post_date) ?></p>
                    <label class="title-h-berita"><?= $post->title ?></label>

                    <div class="content-galeri-berita">
                        <?= $post->content ?>
                    </div>
                </div>

                <?php
                endsection();
            }
        ?>
    <?php
        } else {
        ?>
            <?php show_404(); ?>
        <?php
    }
    ?>
<?php ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>P2BJ</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/pgw-slideshow-master/pgwslideshow.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/css/style.css') ?>">
</head>
<body>    
    <?php render('content') ?>

    <script type="text/javascript" src="<?= base_url('public/js/jquery-1.11.2.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('public/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- Gallery Img -->
    <script type="text/javascript" src="<?= base_url('public/plugins/pgw-slideshow-master/pgwslideshow.min.js') ?>"></script>

    <?php render('page_script') ?>
</body>
</html>