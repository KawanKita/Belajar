<?php section('content') ?>
    <div class="content-body-c">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-1 col-sm-2 col-xs-12 left-menu-galeri">
					<ul>
						<li>
							<?= form()->select('year', getLists('_year'), $year) ?>
						</li>    
                        <?php foreach(getConfig('short_month') as $key => $row) { ?>
                            <li><a class="<?= ($key == $month) ? 'active' : '' ?>" href="<?= base_url($this->uri->segment(1) .'/'.$year.'/'.$key) ?>"><?= $row ?></a></li>
                        <?php } ?>
    				</ul>
				</div>
				<div class="col-md-4 col-sm-10 col-xs-12 center-menu" id="boxscroll" style="padding: 5px; overflow: auto; height: 550px;">
					<ul>
						<li>
        					<p class="title-center-galeri"><?= getConfig('post_type')[$type] ?></p>
        				</li>

                        <?php $posts = CMS()->posts()->get_post_archive($type, $year.'-'.$month) ?>
                        <?php if (count($posts) > 0) { ?>
        					<?php foreach($posts as $post) { ?>
                                <li>
            						<a href="<?= base_url('archive/detail/'. $post->slug) ?>" target="detail_archive">
                                        <?php if ($post->main_image) { ?>
                							<div class="img-tumbnail-berita">
                								<img style="height: 68px;" src="<?= $post->main_image ?>">
                							</div>
                                        <?php } ?>
            							<span class="tgl-title"><?= locale_human_date($post->post_date) ?></span>
            							<p class="title-center"><?= $post->title ?></p>
                                        <br />
            						</a>
            					</li>
                            <?php } ?>
                        <?php } else { ?>                            
                            <li>
                                <a href="javascript:void(0)"><p class="title-center">Tidak ada konten.</p></a>
                            </li>
                        <?php } ?>
					</ul>
				</div>
				<div class="col-md-7 col-sm-12 col-xs-12 right-content" id="boxscroll1" style="padding: 5px; overflow: auto; height: 550px;">
                    <!-- Repeat content berita -->
                    <?php 
                        if(!$slug) {
                            $post = CMS()->posts()->latest($type, 1, 0);
                            if ($post) {
                                $slug = $post->slug;
                    ?>
                                <iframe name="detail_archive" src="<?= base_url('archive/detail/' . $slug) ?>" style="width:100%;height: 550px;" border="0"></iframe>                   
                    <?php
                            } else {

                            }
                        } else {
                    ?>
                        <iframe name="detail_archive" src="<?= base_url('archive/detail/' . $slug) ?>" style="width:100%;height: 550px;" border="0"></iframe>                   
                    <?php
                        }
                    ?>                    
                    <!-- End repeat -->
        		</div>
			</div>
		</div>
	</div>

<footer class="footer hidden-sm hidden-xs">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>
<?php endsection() ?>

<?php getview('layouts/layout') ?>