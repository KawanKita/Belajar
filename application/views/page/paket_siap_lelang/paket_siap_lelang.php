<?php section('content') ?>

	<div class="container-fluid">
		<div class="content-body">
			<div class="container">
				<div class="title-form light-grey margin-plus">Paket Siap Lelang</div>
				<div class="auction-info">
					<div class="auction-info-search">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<input name="name" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Masukkan kode atau nama lelang" id="input-search-p" />
							</div>
							<div class="col-md-6 col-sm-12 input-select">
								<label class="col-md-5 control-label light-grey letter-spacing text-right">KATEGORI</label>
					            <div class="col-md-7 select-dropdown" data-settings='{"wrapperClass":"metro"}'>
					             	<select class="dropdown">
					             		<!-- Repeat Kategori Paket Siap Lelang -->
									    <option value="" class="label">Semua</option>
									    <option value="1">Option 1</option>
									    <option value="2">Option 2</option>
									    <option value="3">Option 3</option>
									    <option value="4">Option 4</option>
									    <option value="5">Option 5</option>
									    <!-- End Repeat -->
									</select> 
					            </div>
							</div>
						</div>
					</div>
					<p class="light-grey icon-paket-siap-lelang">
						<span class="paket-siap-lelang-sub-title light-grey">Pengumuman</span> <i class="fa fa-circle"></i> 
						<span class="paket-siap-lelang-sub-title light-grey">Peserta</span> <i class="fa fa-circle"></i> 
						<span class="paket-siap-lelang-sub-title light-grey">Harga Penawaran</span>
					</p>
					<p class="hr"></p>

					<!-- Repeat Paket siap lelang -->
					<div class="row">
						<div class="col-md-5 col-xs-12 col-1">
							<p class="paket-siap-lelang-title light-grey">Pengadaan Belanja Perlengkapan PON 2016</p>
							<table class="light-grey">
								<tr>
									<td style="width: 130px">Katergori</td>
									<td>: Pengadaan Barang</td>
								</tr>
								<tr>
									<td>Jenis Lelang</td>
									<td>: e-Lelang Umum</td>
								</tr>
								<tr>
									<td>Metode</td>
									<td>: Pascakualifikasi Satu File - Sistem Gugur</td>
								</tr>
							</table>
						</div>
						<div class="col-md-3 col-xs-12 col-2 light-grey">
							<p>Komite Olahraga</p>
							<p>Nasional Indonesia</p>
						</div>
						<div class="col-md-3 col-xs-12 col-3 light-grey">
							<p><i class="ion-checkmark-circled"></i> Pengumuman Pascakualifikasi</p>
							<p><i class="ion-checkmark-circled"></i> Download Dkumen Pengadaan</p>
						</div>
						<div class="col-md-1 col-xs-12 col-4 light-grey">
							<p>899,9 jt</p>
						</div>
						<div class="col-xs-12">
							<p class="press">
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalInformasiLelang">Pengumuman</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalPeserta">Peserta</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalHarga">Harga Penawaran</button>
							</p>
						</div>
					</div>
					<p class="hr"></p>
					<!-- End repeat -->
					
					<div class="row">
						<div class="col-md-5 col-xs-12 col-1">
							<p class="paket-siap-lelang-title light-grey">Pengadaan Belanja Perlengkapan PON 2016</p>
							<table class="light-grey">
								<tr>
									<td style="width: 130px">Katergori</td>
									<td>: Pengadaan Barang</td>
								</tr>
								<tr>
									<td>Jenis Lelang</td>
									<td>: e-Lelang Umum</td>
								</tr>
								<tr>
									<td>Metode</td>
									<td>: Pascakualifikasi Satu File - Sistem Gugur</td>
								</tr>
							</table>
						</div>
						<div class="col-md-3 col-xs-12 col-2 light-grey">
							<p>Komite Olahraga</p>
							<p>Nasional Indonesia</p>
						</div>
						<div class="col-md-3 col-xs-12 col-3 light-grey">
							<p><i class="ion-checkmark-circled"></i> Pengumuman Pascakualifikasi</p>
							<p><i class="ion-checkmark-circled"></i> Download Dkumen Pengadaan</p>
						</div>
						<div class="col-md-1 col-xs-12 col-4 light-grey">
							<p>899,9 jt</p>
						</div>
						<div class="col-xs-12">
							<p class="press">
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalInformasiLelang">Pengumuman</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalPeserta">Peserta</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalHarga">Harga Penawaran</button>
							</p>
						</div>
					</div>
					<p class="hr"></p>
					<div class="row">
						<div class="col-md-5 col-xs-12 col-1">
							<p class="paket-siap-lelang-title light-grey">Pengadaan Belanja Perlengkapan PON 2016</p>
							<table class="light-grey">
								<tr>
									<td style="width: 130px">Katergori</td>
									<td>: Pengadaan Barang</td>
								</tr>
								<tr>
									<td>Jenis Lelang</td>
									<td>: e-Lelang Umum</td>
								</tr>
								<tr>
									<td>Metode</td>
									<td>: Pascakualifikasi Satu File - Sistem Gugur</td>
								</tr>
							</table>
						</div>
						<div class="col-md-3 col-xs-12 col-2 light-grey">
							<p>Komite Olahraga</p>
							<p>Nasional Indonesia</p>
						</div>
						<div class="col-md-3 col-xs-12 col-3 light-grey">
							<p><i class="ion-checkmark-circled"></i> Pengumuman Pascakualifikasi</p>
							<p><i class="ion-checkmark-circled"></i> Download Dkumen Pengadaan</p>
						</div>
						<div class="col-md-1 col-xs-12 col-4 light-grey">
							<p>899,9 jt</p>
						</div>
						<div class="col-xs-12">
							<p class="press">
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalInformasiLelang">Pengumuman</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalPeserta">Peserta</button>
								<button type="button" class="button-white" data-toggle="modal" data-target="#modalHarga">Harga Penawaran</button>
							</p>
						</div>
					</div>
					<p class="hr"></p>
					<p>
				    	<button type="button" class="button -greywhite">LOAD MORE</button>
				    </p>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Informasi Lelang -->
	<div class="modal fade" id="modalInformasiLelang" role="dialog">
	    <div class="modal-dialog modal-lg">
	     	<div class="modal-content" style="width:auto; overflow-x: auto;">
		        <div class="modal-header border-bottom-pop-up" style="width: 1000px;">
					<span class="pop-up-title">Informasi Lelang</span>
					<span class="pop-up-action pull-right">
						<a href="#">DOWNLOAD <i class="ion-arrow-down-a icon"></i></a>
						<a href="#">PRINT <i class="ion-printer icon"></i></a>
					</span>
		        </div>
		        <div class="modal-body" style="width: 1000px;">
		          	<div class="row">
		          		<div class="col-xs-3 name-pop-up">
							NAMA LELANG
						</div>
						<div class="col-xs-6 description-pop-up">
							Pembangunan Los Jaringan dan Pos Pantau di UPT PP Bulu Kab.Tuban (Pembangunan Fasilitas Darat) TA 2016
						</div>
						<div class="col-xs-2 code-pop-up">
							KODE LELANG<br>
							<span class="no-code">10449015</span>
						</div>
		          	</div>
					<div class="pop-up-table">
						<div class="row">
							<div class="col-xs-2 text-left">
								<div class="right-menu">KETERANGAN</div>
							</div>
							<div class="col-xs-3 col-xs-offset-3 text-left">
							</div>
						</div>
					</div>
					<table class="table table-bordered">
						<tr>
							<td class="left-form-table table-middle">TAHAP LELANG</td>
							<td class="right-form-table table-middle" colspan="3">
								<ul>
									<li><i class="ion-checkmark-circled"></i> Pengumuman Pascakualifikasi</li>
									<li><i class="ion-checkmark-circled"></i> Download Dokumen Pengadaan</li>
									<li><i class="ion-checkmark-circled"></i> Upload Dokumen Penawaran</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">INSTANSI</td>
							<td class="right-form-table table-middle" colspan="3">Provinsi Jawa Timur</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">SATUAN KERJA</td>
							<td class="right-form-table table-middle" colspan="3">Dinas Perikanan dan Kelautan Provinsi Jawa Timur</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">KATEGORI</td>
							<td class="right-form-table table-middle" colspan="3">Pekerjaan Kontruksi</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">METODE PENGADAAN</td>
							<td class="right-form-table table-middle">e-Lelang Pemilihan Langsung</td>
							<td class="left-form-table table-middle">METODE PENGADAAN</td>
							<td class="right-form-table table-middle">Pascakualifikasi</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">METODE DOKUMEN</td>
							<td class="right-form-table table-middle">Satu File</td>
							<td class="left-form-table table-middle">METODE DOKUMEN</td>
							<td class="right-form-table table-middle">Sistem Gugur</td>
						</tr>
					</table>

					<div class="pop-up-table">
						<div class="row">
							<div class="col-xs-2 text-left">
								<div class="right-menu">ANGGARAN</div>
							</div>
							<div class="col-xs-3 col-xs-offset-3 text-left">
							</div>
						</div>
					</div>
					<table class="table table-bordered">
						<tr>
							<td class="left-form-table table-middle">NILAI PAGU PAKET</td>
							<td class="right-form-table table-middle">Rp. 842.252.200,00</td>
							<td class="left-form-table table-middle">NILAI HPS PAKET</td>
							<td class="right-form-table table-middle">Rp. 839.508.808,00</td>
						</td>
					</tr>
					<tr>
						<td class="left-form-table table-middle" rowspan="4">JENIS KONTRAK</td>
					</tr>
					<tr>
						<td class="left-form-table table-middle">CARA PEMBAYARAN</td>
						<td class="right-form-table table-middle" colspan="2"> Harga Satuan</td>
					</tr>
					<tr>
						<td class="left-form-table table-middle">PEMBEBANAN TAHUN ANGGARAN</td>
						<td class="right-form-table table-middle" colspan="2">Tahun Tungaal</td>
					</tr>
					<tr>
						<td class="left-form-table table-middle">SUMBER PENGADAAN</td>
						<td class="right-form-table table-middle" colspan="2">Pengadaan Tunggal</td>
					</tr>
					</table>
					<table class="table table-bordered">
						<tr>
							<td class="left-form-table table-middle">KUALIFIKASI USAHA</td>
							<td class="right-form-table table-middle" colspan="3">Perusahaan Kecil</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">LOKASI PEKERJAAN</td>
							<td class="right-form-table table-middle" colspan="3">PP Bulu Tuban, Kab.Tuban</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle" rowspan="4">SYARAT KUALIFIKASI</td>
						</tr>
						<tr>
							<td class="right-form-table table-middle" colspan="3">
								Ijin Usaha : SBU, SIUP, TDP, AKTE
								<p>
									Klasifikasi : Peserta yang berbadan usaha harus memiliki SIUP Bidang Perdagangan, Sup Bidang Perlengkapan Olah Raga, Kualifikasi : Usaha Non Kecil yang masih berlaku
								</p> 
							</td>
						</tr>
						<tr>
							<td class="right-form-table table-middle" colspan="3">
								<ol type="1">
									<li>1. Telah melunasi kewajiban pajak tahun terakhir (SPT/PPh)</li>
									<li>2. Memiliki NPWP dan telah memenuhi kewajiban perpajakan tahun pajak terakhir (SPT Tahun 2015)</li>
								</ol> 
							</td>
						</tr>
						<tr>
							<td class="right-form-table table-middle" colspan="3">Persyaratan Lain Sesuai Dokumen Penagadaan</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">PESERTA LELANG</td>
							<td class="right-form-table table-middle" colspan="3">13 Peserta <a href="peserta_lelang.php">[detil] <i class="ion-arrow-right-c"></i></a></td>
						</tr>
						<tr>
							<td class="left-form-table table-middle" rowspan="3">DOKUMEN LAIN</td>
						</tr>
						<tr>
							<td class="right-form-table table-middle">Dokumen Pelengkap 1.pdf</td>
							<td class="right-form-table table-middle">TGL KIRIM : 17 Mei 2016</td>
						</tr>
						<tr>
							<td class="right-form-table table-middle">Dokumen Pelengkap 1.pdf</td>
							<td class="right-form-table table-middle">TGL KIRIM : 17 Mei 2016</td>
						</tr>
					</table>
		        </div>
		        <div class="modal-footer border-top-pop-up" style="width: 1000px;">
		        	<span class="pop-up-footer pull-left">&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		        	</span>
					<button type="button" data-dismiss="modal" class="button button-pop-up -whitegrey pull-right">TUTUP</button>
		        </div>
	      	</div>
	    </div>
	</div>

	<!-- Modal Peserta -->
	<div class="modal fade" id="modalPeserta" role="dialog">
	    <div class="modal-dialog modal-lg">
	     	<div class="modal-content" style="width:auto; overflow-x: auto;">
		        <div class="modal-header border-bottom-pop-up" style="width: 1000px;">
					<span class="pop-up-title">Peserta Lelang</span>
					<span class="pop-up-action pull-right">
						<a href="#">DOWNLOAD <i class="ion-arrow-down-a icon"></i></a>
						<a href="#">PRINT <i class="ion-printer icon"></i></a>
					</span>
		        </div>
		        <div class="modal-body" style="width: 1000px;">
		          	<div class="row">
		          		<div class="col-xs-3 name-pop-up">
							NAMA LELANG
						</div>
						<div class="col-xs-6 description-pop-up">
							Pembangunan Los Jaringan dan Pos Pantau di UPT PP Bulu Kab.Tuban (Pembangunan Fasilitas Darat) TA 2016
						</div>
						<div class="col-xs-2 code-pop-up">
							KODE LELANG<br>
							<span class="no-code">10449015</span>
						</div>
		          	</div>
					<div class="pop-up-table">
						<div class="row">
							<div class="col-xs-1 text-left">
								<div class="pop-up-right-menu-no">NO</div>
							</div>
							<div class="col-xs-3 col-xs-offset-3 text-left">
								<div class="left-menu-peserta">HARGA PENAWARAN</div>
							</div>
						</div>
					</div>
					<table class="table table-bordered">
						<tr>
							<td class="left-form-table-peserta table-middle">1</td>
							<td class="right-form-table-peserta-lelang table-middle-peserta">Penyedia 1</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">2</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 2</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">3</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 3</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">4</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 4</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">5</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 5</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">6</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 6</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">7</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 7</td>
						</tr>
						<tr>
							<td class="left-form-table-peserta table-middle">8</td>
							<td class="right-form-table-peserta table-middle-peserta">Penyedia 8</td>
						</tr>
					</table>
		        </div>
		        <div class="modal-footer border-top-pop-up" style="width: 1000px;">
		        	<span class="pop-up-footer pull-left">&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		        	</span>
					<button type="button" data-dismiss="modal" class="button button-pop-up -whitegrey pull-right">TUTUP</button>
		        </div>
	      	</div>
	    </div>
	</div>

	<!-- Modal Harga -->
	<div class="modal fade" id="modalHarga" role="dialog">
	    <div class="modal-dialog modal-lg">
	     	<div class="modal-content" style="width:auto; overflow-x: auto;">
		        <div class="modal-header border-bottom-pop-up" style="width: 1000px;">
					<span class="pop-up-title">Harga Penawaran</span>
					<span class="pop-up-action pull-right">
						<a href="#">DOWNLOAD <i class="ion-arrow-down-a icon"></i></a>
						<a href="#">PRINT <i class="ion-printer icon"></i></a>
					</span>
		        </div>
		        <div class="modal-body" style="width: 1000px;">
		          	<div class="row">
		          		<div class="col-xs-3 name-pop-up">
							NAMA LELANG
						</div>
						<div class="col-xs-6 description-pop-up">
							Pembangunan Los Jaringan dan Pos Pantau di UPT PP Bulu Kab.Tuban (Pembangunan Fasilitas Darat) TA 2016
						</div>
						<div class="col-xs-3 code-pop-up">
							KODE LELANG<br>
							<span class="no-code">10449015</span>
						</div>
		          	</div>
					<div class="pop-up-table">
						<div class="row">
							<div class="col-xs-3 text-left">
								<div class="right-menu">NAMA PENYEDIA</div>
							</div>
							<div class="col-xs-3 col-xs-offset-1 text-left">
								<div class="left-menu">HARGA PENAWARAN</div>
							</div>
						</div>
					</div>
					<table class="table table-bordered">
						<tr>
							<td class="left-form-table table-middle">Penyedia 1</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 2</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 3</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 4</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 5</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 6</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 7</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
						<tr>
							<td class="left-form-table table-middle">Penyedia 8</td>
							<td class="right-form-table table-middle">Rp 125.000.000.000</td>
						</tr>
					</table>
		        </div>
		        <div class="modal-footer border-top-pop-up" style="width: 1000px;">
		        	<span class="pop-up-footer pull-left">&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		        	</span>
					<button type="button" data-dismiss="modal" class="button button-pop-up -whitegrey pull-right">TUTUP</button>
		        </div>
	      	</div>
	    </div>
	</div>


<footer class="footer hidden-sm hidden-xs">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<?php endsection(); ?>

<?php getview('layouts/layout'); ?>