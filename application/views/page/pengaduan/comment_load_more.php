<?php
	$page = $this->input->get('page');
	$page = !is_numeric($page) ? 1 : $page;
	$key = $this->input->get('key');
	$per_page = 8;
	$comments = CMS()->comments()->search($this->input->get('key'), $per_page, ($page-1) * $per_page);
?>
<?php foreach($comments as $row) { ?>
    <div class="news-bottom-box-date light-grey writting-dintance"><?= locale_human_date($row->comment_date); ?></div>
    <div class="news-complaint-text grey-c"><?= $row->comment; ?></div>
    <div class="news-bottom-box-date grey"><i><?= $row->email; ?></i></div>
    <div class="news-complaint-answer">Jawaban :</div>
    <div class="news-complaint-text-answer grey">
        <?php $reply = CMS()->comments()->reply($row->id, 'pengaduan', 1, 0); ?>
        <?php if ($reply) { ?>
            <?= $reply->comment; ?>
        <?php } else { ?>
            Belum terjawab.
        <?php } ?>
        <p class="hr"></p>
    </div>
<?php } ?>
<?php if ($comments) { ?>
    <button type="button" id="btn_more" name="btn_more" class="button -greywhite" onclick="loadMore()">LOAD MORE</button>
<?php } ?>
    