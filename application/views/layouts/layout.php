<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>P2BJ</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/pgw-slideshow-master/pgwslideshow.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/ionicons-2.0.1/css/ionicons.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/easydropdown-master/themes/easydropdown.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/plugins/easydropdown-master/themes/easydropdown.metro.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('public/css/style.css') ?>">
</head>
<body>
<div class="container-j">
    <nav class="navbar navbar-default hidden-xs hidden-sm" style="background: url('<?= base_url() ?>public/img/nav-bg.jpg') no-repeat left; background-color: #EB2228;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span style="background: url('<?= base_url() ?>public/img/nav-bg.jpg') no-repeat center; height: 50px;"></span>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= base_url(); ?>">HOME</a></li>
                    <li><a href="<?= base_url('paket_siap_lelang'); ?>">PAKET SIAP LELANG</a></li>
                    <li><a href="<?= base_url('proses_lelang'); ?>">PROSES LELANG</a></li>
                    <li><a href="<?= base_url('profil'); ?>">PROFILE</a></li>
                    <li><a href="<?= base_url('galeri'); ?>">GALERI</a></li>
                    <li>
                        <form action="<?= base_url('search') ?>" target="_self">
                            <div class="search">
                                <input name="key" id="input-search" type="text" value="" class="input-search" placeholder="Search"/>
                            </div>
                        </form>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <nav class="navbar navbar-default hidden-lg hidden-md hidden-xs" style="background: url('<?= base_url() ?>public/img/nav-bg.jpg') no-repeat left; background-color: #EB2228;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span style="background: url('<?= base_url() ?>public/img/nav-bg.jpg') no-repeat center; height: 50px;"></span>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">PAKET SIAP <br /> LELANG</a></li>
                    <li><a href="#">PROSES <br /> LELANG</a></li>
                    <li><a href="#">PROFILE</a></li>
                    <li><a href="#">GALERI</a></li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">SEARCH <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <form action="#">
                                    <div class="search">
                                        <input name="key" id="input-search-sm" type="text" value="#" class="input-search" placeholder="Search"/>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <?php render('content') ?>

</div>
<script type="text/javascript" src="<?= base_url('public/js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- CHARTJS CHARTS -->
<script type="text/javascript" src="<?= base_url('public/plugins/chartjs/Chart.min.js') ?>"></script>
<!-- FLOT CHARTS -->
<script type="text/javascript" src="<?= base_url('public/plugins/flot/jquery.flot.min.js') ?>"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script type="text/javascript" src="<?= base_url('public/plugins/flot/jquery.flot.resize.min.js') ?>"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script type="text/javascript" src="<?= base_url('public/plugins/flot/jquery.flot.pie.min.js') ?>"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script type="text/javascript" src="<?= base_url('public/plugins/flot/jquery.flot.categories.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('public/plugins/easydropdown-master/jquery.easydropdown.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('public/plugins/nicescoll/jquery.nicescroll.min.js') ?>" type="text/javascript"></script>
<!-- Gallery Img -->
<script type="text/javascript" src="<?= base_url('public/plugins/pgw-slideshow-master/pgwslideshow.min.js') ?>"></script>

<script type="text/javascript" src="<?= base_url('public/js/app.js') ?>"></script>

<?php render('page_script') ?>

</body>
</html>