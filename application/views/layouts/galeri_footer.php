  </div>
  <script type="text/javascript" src="<?= base_url() ?>public/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- CHARTJS CHARTS -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/chartjs/Chart.min.js"></script>
  <!-- FLOT CHARTS -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/flot/jquery.flot.min.js"></script>
  <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/flot/jquery.flot.resize.min.js"></script>
  <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/flot/jquery.flot.pie.min.js"></script>
  <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/flot/jquery.flot.categories.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/easydropdown-master/jquery.easydropdown.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/nicescoll/jquery.nicescroll.min.js" type="text/javascript"></script>
  <!-- Gallery Img -->
  <script type="text/javascript" src="<?= base_url() ?>public/plugins/pgw-slideshow-master/pgwslideshow.min.js"></script>


  <!-- Page script -->
  <script type="text/javascript">

    $(document).ready(function() {
      $('.pgwSlideshow').pgwSlideshow();
    });
  </script>
</body>
</html>