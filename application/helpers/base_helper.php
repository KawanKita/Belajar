<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function getConfig($key) {
	$CI = &get_instance();
	return $CI->config->item($key);
}

function getLists($key, $placeholder = null) {
	$CI = &get_instance();
	$data = $CI->config->item($key);
	$lists = array();
	if ($placeholder) {
		$lists[''] = $placeholder;
	}
	$lists = array_merge($lists, $data);
	return $lists;
}


function getList($key, $value) {
	$CI = &get_instance();
	$data = $CI->config->item($key);
	if (isset($data[$value])) {
		$result = $data[$value];
	} else {
		$result = null;
	}
	return $result;
}