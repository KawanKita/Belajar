<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Av
 * Date: 18/10/2016
 * Time: 19:33
 */

function content_limiter($content, $limit) {	
    if (strlen($content) > $limit) {
        return word_limiter(character_limiter($content, $limit), str_word_count(character_limiter($content, $limit)) - 1);
    } else {
        return $content;
    }
}

function post_year($post_date) {
	return date('Y', strtotime($post_date));
}

function post_month($post_date) {
	return date('m', strtotime($post_date));
}

function post_archive($post_date) {
	return post_year($post_date).'/'.post_month($post_date); 
}