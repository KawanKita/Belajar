<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function tree_data($data, $id, $parent, $start = '')   {
	$tree = array();
	foreach ($data as $row) {
		$tree[$row->$parent][] = (Object) $row;
	}

	$result = set_tree_data($tree, $tree[$start], $id, $parent);
	return $result;
}

function set_tree_data($data, $parent_data, $id, $parent, $level = 0) {
	$result = array();
	foreach ($parent_data as $key => $row) {
		$row->tree_level = $level;
		$result[] = $row;
		if (isset($data[$row->$id])) {
			$result = array_merge($result, set_tree_data($data, $data[$row->$id], $id, $parent_data, $level + 1));
			unset($data[$row->id]);
		}
	}
	return $result;
}